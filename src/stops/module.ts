import StopController from "./api/controller/StopController";
import StopGroupController from "./api/controller/StopGroupController";
import { Router } from "express";
import validate from 'express-validation';
import { createStopValidation } from "./validation/StopRequestValidation";

const stopController = new StopController();
const stopGroupController = new StopGroupController();

const routes: Router = Router();

// stops
routes.get('/nearest', stopController.getNearestStopGroup);
routes.post('/bounds', stopController.getByBounds);
routes.get('/search', stopController.searchStopGroupsWithAddress);
routes.delete('/:stopId', stopController.deleteById);
routes.post('/create', validate(createStopValidation), stopController.createStop);

// stop groups 
routes.get('/groups/search', stopController.searchStopGroupsByName);
routes.get('/groups/:stopGroupId', stopGroupController.getById);
routes.delete('/groups/:stopGroupId', stopGroupController.deleteById);
routes.delete('/groups/:stopGroupId/:stopId', stopGroupController.removeStopFromGroup);
routes.patch('/groups/:stopGroupId/:stopId', stopGroupController.addStopToGroup);

export { routes };