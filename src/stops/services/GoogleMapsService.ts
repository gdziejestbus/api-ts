import GoogleMapsClient from '@google/maps';
import CONFIG from '../../shared/config';
import randomstring from 'randomstring';

const googleMapsClient = GoogleMapsClient.createClient({
	key: CONFIG.GOOGLE_API_KEY,
	Promise: Promise
});

export const getAddressDetails = async (address: string): Promise<any[]> => {
	const data = await googleMapsClient.placesAutoComplete({ input: address, language: "pl", sessiontoken: randomstring.generate(7) }).asPromise();

	const promises = [];

	for (const place of data.json.predictions) {
		promises.push(googleMapsClient.place({ placeid: place.place_id, language: "pl" }).asPromise());
	}

	const results = await Promise.all(promises);

	const places = results.map(result => result.json.result);

	return await formatAdressessFromGoogleAPI(places);
}

export const getAddressFromCoordinates = async (coordinates: any): Promise<any> => {
	const response = await googleMapsClient.reverseGeocode({ latlng: coordinates, language: "pl" }).asPromise();

	const formattedResponse = await formatAdressessFromGoogleAPI(response.json.results);

	return formattedResponse[0] ? formattedResponse[0].name : null;
}

export const getStopInfoFromCoordinates = async (coordinates: any): Promise<any> => {
	const response = await googleMapsClient.reverseGeocode({ latlng: coordinates, language: "pl" }).asPromise();

	const formattedResponse = await formatAdressessFromGoogleAPI(response.json.results);

	return formattedResponse[0] ? {
		county: formattedResponse[0].details.county,
		city: formattedResponse[0].details.city,
		voivodeship: formattedResponse[0].details.voivodeship
	} : null;
}

const formatAdressessFromGoogleAPI = async (addresses: Array<any>): Promise<Array<any>> => {
	const formattedAddresses = [];

	for (let i = 0; i < addresses.length; i++) {
		const address = addresses[i];

		const formattedAddress: any = {};

		address.address_components.forEach(component => {
			formattedAddress[component.types[0]] = component.short_name ? component.short_name : component.long_name;
		});

		const placeName = await getPlaceName(address);

		formattedAddresses.push(getFormattedAddressObject(formattedAddress, address, placeName));
	}

	return formattedAddresses;
}

const getFormattedAddressObject = (formattedAddress, address, placeName) => {
	return {
		name: getString(formattedAddress, placeName),
		details: {
			number: formattedAddress.street_number || null,
			premise: formattedAddress.premise || null,
			street: formattedAddress.route || null,
			city: formattedAddress.locality || null,
			county: formattedAddress.administrative_area_level_2 || null,
			voivodeship: firstLetterUppercase(formattedAddress.administrative_area_level_1) || null,
			placeName
		},
		location: {
			type: "Point",
			coordinates: [address.geometry.location.lat, address.geometry.location.lng]
		}
	}
}

const getPlaceName = async (address) => {
	return address.types.includes("establishment") ? await getPlaceDetails(address.place_id) : null;
}

const getPlaceDetails = async (id) => {
	const place = await googleMapsClient.place({ placeid: id, language: "pl" }).asPromise();

	return place.json.result.name;
}

const getString = (formattedAddress, placeName): string => {
	let text = "";

	if (placeName) text += placeName + ", ";
	if (formattedAddress.route) text += formattedAddress.route;
	if (formattedAddress.street_number) text += " " + formattedAddress.street_number;
	if (text.length > 0) text += ", ";
	if (formattedAddress.locality) text += formattedAddress.locality;
	if (formattedAddress.premise) text += " " + formattedAddress.premise;
	if (formattedAddress.administrative_area_level_2 && formattedAddress.administrative_area_level_2 !== formattedAddress.locality) text += ", " + formattedAddress.administrative_area_level_2;
	if (formattedAddress.administrative_area_level_1) text += ", " + firstLetterUppercase(formattedAddress.administrative_area_level_1);

	return text.replace(" , ", " ");
}

const firstLetterUppercase = (string) => {
	return string ? string.charAt(0).toUpperCase() + string.slice(1) : string;
}