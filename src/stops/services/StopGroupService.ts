import StopGroup from "../models/StopGroup";
import StopGroupRepository, { IStopGroupRepository } from "../repositories/StopGroupRepository";

export interface IStopGroupService {
	createStopGroup(stopGroup: StopGroup): Promise<StopGroup>;
	getById(id: string): Promise<StopGroup>;
	deleteById(id: string): Promise<void>;
	update(stopGroup: StopGroup): Promise<void>;
	addStopToGroup(stopGroupId: string, stopId: string): Promise<void>;
	removeStopFromGroup(stopGroupId: string, stopId: string): Promise<void>;
	getStopGroupByName(stopGroupName: string): Promise<StopGroup>;
	findByNameAndCounty(name: string, county: string): Promise<StopGroup>;
	getNearestStopGroup(coords: number[], maxDistance: number): Promise<StopGroup>;
	getByBounds(coords: Array<number[]>): Promise<Array<StopGroup>>;
	findByName(text: string): Promise<Array<StopGroup>>;
}

export default class StopGroupService implements IStopGroupService {
	private stopGroupRepository: IStopGroupRepository;

	constructor() {
		this.stopGroupRepository = new StopGroupRepository();
	}

	public async getById(id: string): Promise<StopGroup> {
		const stopGroup = await this.stopGroupRepository.getById(id);

		if (!stopGroup) throw new Error("NotFound");

		return stopGroup;
	}

	public async deleteById(id: string): Promise<void> {
		await this.stopGroupRepository.deleteById(id);
	}

	public async update(stopGroup: StopGroup): Promise<void> {
		const existingStopGroup = await this.stopGroupRepository.getById(stopGroup.id);

		if (!existingStopGroup) throw new Error("NotFound");

		const updatedStopGroup = {
			...existingStopGroup,
			...stopGroup
		}

		await this.stopGroupRepository.update(updatedStopGroup);
	}

	public async createStopGroup(stopGroup: StopGroup): Promise<StopGroup> {
		const createdStopGroup = await this.stopGroupRepository.create(stopGroup);

		return createdStopGroup;
	}

	public async addStopToGroup(stopGroupId: string, stopId: string): Promise<void> {
		const stopGroup = await this.stopGroupRepository.getById(stopGroupId);

		if (!stopGroup) throw new Error("NotFound");

		const updatedStopGroup: StopGroup = {
			...stopGroup,
			stopsId: stopGroup.stopsId.concat(stopId)
		}

		await this.stopGroupRepository.update(updatedStopGroup);
	}

	public async removeStopFromGroup(stopGroupId: string, stopId: string): Promise<void> {
		const stopGroup = await this.stopGroupRepository.getById(stopGroupId);

		if (!stopGroup) throw new Error("NotFound");

		const updatedStopsId = stopGroup.stopsId;

		if (stopGroup.stopsId.includes(stopId))
			updatedStopsId.splice(stopGroup.stopsId.indexOf(stopId), 1);

		const updatedStopGroup: StopGroup = {
			...stopGroup,
			stopsId: updatedStopsId
		}

		await this.stopGroupRepository.update(updatedStopGroup);
	}

	public async getStopGroupByName(stopGroupName: string): Promise<StopGroup> {
		const stopGroup = await this.stopGroupRepository.getByName(stopGroupName);

		if (!stopGroup) throw new Error("NotFound");

		return stopGroup;
	}

	public async findByNameAndCounty(name: string, county: string): Promise<StopGroup> {
		return await this.stopGroupRepository.findByNameAndCounty(name, county);
	}

	public async getNearestStopGroup(coords: number[], maxDistance: number): Promise<StopGroup> {
		return await this.stopGroupRepository.getNearestStopGroup(coords, maxDistance);
	}

	public async getByBounds(coords: Array<number[]>): Promise<Array<StopGroup>> {
		if (coords.length < 2) throw new Error("InsufficientData");

		return await this.stopGroupRepository.getByBounds(coords);
	}

	public async findByName(text: string): Promise<Array<StopGroup>> {
		return await this.stopGroupRepository.findByName(text);
	}
}