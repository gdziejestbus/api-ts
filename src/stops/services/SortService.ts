import compareStrings from 'compare-strings';

export const sortResults = (results, text) => {
	return results.sort((a, b) => {
		if (compareStrings(a.name, text) < compareStrings(b.name, text)) return 1;
		if (compareStrings(a.name, text) > compareStrings(b.name, text)) return -1;
		return 0;
	});
}