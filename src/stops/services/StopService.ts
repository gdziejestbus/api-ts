import Stop from "../models/Stop";
import StopRepository, { IStopRepository } from "../repositories/StopRepository";
import CreateStop from "../models/CreateStop";
import StopGroupService, { IStopGroupService } from "./StopGroupService";
import StopGroup from "../models/StopGroup";
import StopGroupDetailed from "../models/StopGroupDetailed";
import { getAddressDetails, getStopInfoFromCoordinates } from "./GoogleMapsService";
import { AddressDetailed } from "../models/AddressDetailed";

export interface IStopService {
	createStop(stop: CreateStop): Promise<string>;
	getById(id: string): Promise<Stop>;
	deleteById(id: string): Promise<void>;
	update(stop: Stop): Promise<void>;
	getMany(calendarsId: Array<string>): Promise<Array<Stop>>;
	getNearest(coords: number[], maxDistance: number): Promise<StopGroupDetailed>;
	getByBounds(coords: Array<number[]>): Promise<Array<StopGroupDetailed>>;
	searchStopGroupsWithAddresses(text: string): Promise<any>;
	searchStopGroupsByName(name: string): Promise<Array<StopGroupDetailed>>;
}

export default class StopService implements IStopService {
	private stopRepository: IStopRepository;
	private stopGroupService: IStopGroupService;

	constructor() {
		this.stopRepository = new StopRepository();
		this.stopGroupService = new StopGroupService();
	}

	public async getById(id: string): Promise<Stop> {
		const stop = await this.stopRepository.getById(id);

		if (!stop) throw new Error("NotFound");

		return stop;
	}

	public async deleteById(id: string): Promise<void> {
		await this.stopRepository.deleteById(id);
	}

	public async update(stop: Stop): Promise<void> {
		const existingStop = await this.stopRepository.getById(stop.id);

		if (!existingStop) throw new Error("NotFound");

		const updatedStop = {
			...existingStop,
			...stop
		}

		await this.stopRepository.update(updatedStop);
	}

	public async createStop(createStop: CreateStop): Promise<string> {
		const stop = {
			name: createStop.name,
			location: createStop.location,
			active: false
		}
		const location = await getStopInfoFromCoordinates(createStop.location.coordinates.reverse());

		const createdStop = await this.stopRepository.create(stop);

		const existingStopGroup = await this.stopGroupService.findByNameAndCounty(createStop.name, location.county);

		if (existingStopGroup) {
			await this.stopGroupService.addStopToGroup(existingStopGroup.id, createdStop.id);
		} else {
			const stopGroup: StopGroup = {
				name: createStop.name,
				voivodeship: location.voivodeship,
				county: location.county,
				location: createStop.location,
				active: false,
				city: location.city,
				stopsId: [createdStop.id]
			}

			await this.stopGroupService.createStopGroup(stopGroup);
		}

		return createdStop.id;
	}

	public async getMany(stopsId: Array<string>): Promise<Array<Stop>> {
		const calendars = await this.stopRepository.findMany(stopsId);

		if (calendars.length === 0) throw new Error("NotFound");

		return calendars;
	}

	public async getNearest(coords: number[], maxDistance: number): Promise<StopGroupDetailed> {
		const nearestStopGroup = await this.stopGroupService.getNearestStopGroup(coords, maxDistance);

		if (!nearestStopGroup) throw new Error("NotFound");

		const stops = await this.stopRepository.findMany(nearestStopGroup.stopsId);

		const StopGroupDetailed: StopGroupDetailed = {
			...nearestStopGroup,
			stops
		}

		return StopGroupDetailed;
	}

	public async getByBounds(coords: Array<number[]>): Promise<Array<StopGroupDetailed>> {
		const stopGroups = await this.stopGroupService.getByBounds(coords);

		const stopsId: Array<string> = [];

		stopGroups.forEach(stopGroup => {
			stopsId.push.apply(stopsId, stopGroup.stopsId);
		});

		const stops = await this.stopRepository.findMany(stopsId);

		return this.getDetailedStopGroups(stopGroups, stops);
	}

	public async searchStopGroupsWithAddresses(text: string): Promise<any> {
		if (!(text.length > 0)) throw new Error("InsufficientData");

		const addressesToReturn = [];

		const addresses = await getAddressDetails(text);
		const stopGroups = await this.stopGroupService.findByName(text);

		for (let i = 0; i < addresses.length; i++) {
			const address = addresses[i];

			const stopGroup = await this.stopGroupService.getNearestStopGroup(address.location.coordinates.reverse(), 10000);

			if (stopGroup) {
				const addressDetailed: AddressDetailed = {
					...address,
					nearestStopGroupId: stopGroup ? stopGroup.id : null,
					nearestStopGroupName: stopGroup ? stopGroup.name : null
				}

				addressesToReturn.push(addressDetailed);
			}
		}

		return {
			stopGroups,
			addresses: addressesToReturn
		};
	}

	public async searchStopGroupsByName(name: string): Promise<Array<StopGroupDetailed>> {
		if (!(name.length > 0)) throw new Error("InsufficientData");
		const stopsId: string[] = [];

		const stopGroups = await this.stopGroupService.findByName(name);

		stopGroups.forEach(stopGroup => {
			stopsId.push.apply(stopsId, stopGroup.stopsId);
		});

		const stops = await this.stopRepository.findMany(stopsId);

		return this.getDetailedStopGroups(stopGroups, stops);
	}

	private getDetailedStopGroups(stopGroups: Array<StopGroup>, stops: Array<Stop>): Array<StopGroupDetailed> {
		const detailedStopGroups: Array<StopGroupDetailed> = [];

		stopGroups.forEach(stopGroup => {
			const selectedStops = stops.filter(x => stopGroup.stopsId.includes(x.id));

			detailedStopGroups.push({
				...stopGroup,
				stops: selectedStops
			})
		});

		return detailedStopGroups;
	}
}