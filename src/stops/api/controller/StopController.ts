import { Response, Request } from "express";
import StopDTO from "../../DTO/StopDTO";
import StopService, { IStopService } from "../../services/StopService";
import { errorHandler } from "../../../shared/errors/errorHandler";
import { CreateStopResponse, GetByIdResponse, GetNearestResponse, GetByBoundsResponse, SearchStopGroupsResponse } from "../../DTO/StopResponses";
import { CreateStopRequest, GetByIdRequest, GetNearestRequest, GetByBoundsRequest } from "../../DTO/StopRequests";
import { toStopDTO } from "../../converters/toStopDTO";
import CreateStop from "../../models/CreateStop";
import { toStopGroupDTO } from "../../converters/toStopGroupDTO";
import { sortResults } from "../../services/SortService";
import { getAddressFromCoordinates } from "../../services/GoogleMapsService";
import StatisticsService, { IStatisticsService } from "../../../statistics/services/StatisticsService";

export interface IStopController {
	createStop(req: Request, res: Response): Promise<Response>;
	getById(req: Request, res: Response): Promise<Response>;
	deleteById(req: Request, res: Response): Promise<Response>;
	getByBounds(req: Request, res: Response): Promise<Response>;
	searchStopGroupsWithAddress(req: Request, res: Response): Promise<Response>;
	searchStopGroupsByName(req: Request, res: Response): Promise<Response>;
}

export default class StopController implements IStopController {
	private stopService: IStopService;
	private statisticsService: IStatisticsService;

	constructor() {
		this.stopService = new StopService();
		this.statisticsService = new StatisticsService();
	}

	public getById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetByIdRequest = { ...req.params };

		try {
			const stop = await this.stopService.getById(request.stopId);

			const stopDTO: StopDTO = toStopDTO(stop);

			const response: GetByIdResponse = { stop: stopDTO };

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public deleteById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetByIdRequest = { ...req.params };

		try {
			await this.stopService.deleteById(request.stopId).catch(err => {
				return res.status(400).json({ Error: err.message });
			});

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public createStop = async (req: Request, res: Response): Promise<Response> => {
		const request: CreateStopRequest = { ...req.body };

		const stop: CreateStop = {
			...request,
			location: {
				type: "Point",
				coordinates: request.coordinates
			}
		};

		try {
			const stopId = await this.stopService.createStop(stop);

			const response: CreateStopResponse = {
				stopId
			}

			return res.status(201).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getNearestStopGroup = async (req: Request, res: Response): Promise<Response> => {
		const request: GetNearestRequest = { ...req.query };

		const coords: number[] = [request.longitude, request.latitude];
		const maxDistance: number = req.query.maxDistance || 10000;

		try {
			const stopGroup = await this.stopService.getNearest(coords, maxDistance);
			const address = await getAddressFromCoordinates({ longitude: request.longitude, latitude: request.latitude });

			const response: GetNearestResponse = {
				...toStopGroupDTO(stopGroup),
				stops: stopGroup.stops.map(stop => toStopDTO(stop)),
				address
			}

			this.statisticsService.addStatistics(req.device.type, req.useragent.browser, "nearest_stop", {
				coordinates: { longitude: request.longitude, latitude: request.latitude },
				nearestStopGroup: stopGroup.name,
				address
			});

			return res.status(200).json(response);
		} catch (err) {
			this.statisticsService.addStatistics(req.device.type, req.useragent.browser, "nearest_stop", {
				coordinates: { longitude: request.longitude, latitude: request.latitude },
				error: true,
				errorMessage: err.message
			});

			return errorHandler(res, err);
		}
	}

	public getByBounds = async (req: Request, res: Response): Promise<Response> => {
		const request: GetByBoundsRequest = { ...req.body };

		const coords: Array<number[]> = request.coords;

		try {
			const stopGroups = await this.stopService.getByBounds(coords);

			const response: GetByBoundsResponse = {
				amount: stopGroups.length,
				stopGroups: stopGroups.map(stopGroup => {
					return {
						...toStopGroupDTO(stopGroup),
						stops: stopGroup.stops.map(stop => toStopDTO(stop))
					}
				})
			}

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public searchStopGroupsWithAddress = async (req: Request, res: Response): Promise<Response> => {
		try {
			const data = await this.stopService.searchStopGroupsWithAddresses(req.query.text);

			const stopGroupsDTO = data.stopGroups.map(stopGroup => toStopGroupDTO(stopGroup));

			const response: SearchStopGroupsResponse = {
				stopGroups: sortResults(stopGroupsDTO, req.query.text),
				addresses: sortResults(data.addresses, req.query.text)
			}

			if (response.stopGroups.length === 0 && response.addresses.length === 0) return res.sendStatus(404);

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public searchStopGroupsByName = async (req: Request, res: Response): Promise<Response> => {
		try {
			const stopGroups = await this.stopService.searchStopGroupsByName(req.query.name);

			const response: GetByBoundsResponse = {
				amount: stopGroups.length,
				stopGroups: stopGroups.map(stopGroup => {
					return {
						...toStopGroupDTO(stopGroup),
						stops: stopGroup.stops.map(stop => toStopDTO(stop))
					}
				})
			}

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}
}