import { Response, Request } from "express";
import StopGroupService, { IStopGroupService } from "../../services/StopGroupService";
import { errorHandler } from "../../../shared/errors/errorHandler";
import { EditStopsIdRequest,GetByIdRequest } from "../../DTO/StopGroupRequests";
import { GetByIdResponse } from "../../DTO/StopGroupResponses";
import { toStopGroupDTO } from "../../converters/toStopGroupDTO";

export interface IStopGroupController {
	getById(req: Request, res: Response): Promise<Response>;
	deleteById(req: Request, res: Response): Promise<Response>;
	addStopToGroup(req: Request, res: Response): Promise<Response>;
	removeStopFromGroup(req: Request, res: Response): Promise<Response>;
}

export default class StopGroupController implements IStopGroupController {
	private stopGroupService: IStopGroupService;

	constructor() {
		this.stopGroupService = new StopGroupService();
	}

	public getById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetByIdRequest = { ...req.params };

		try {
			const stopGroup = await this.stopGroupService.getById(request.stopGroupId);

			const StopGroupDTO = toStopGroupDTO(stopGroup);

			const response: GetByIdResponse = { stopGroup: StopGroupDTO };

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public deleteById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetByIdRequest = { ...req.params };

		try {
			await this.stopGroupService.deleteById(request.stopGroupId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(err, res);
		}
	}

	public addStopToGroup = async (req: Request, res: Response): Promise<Response> => {
		const request: EditStopsIdRequest = {
			stopGroupId: req.params.stopGroupId,
			stopId: req.params.stopId
		}

		try {
			await this.stopGroupService.addStopToGroup(request.stopGroupId, request.stopId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public removeStopFromGroup = async (req: Request, res: Response): Promise<Response> => {
		const request: EditStopsIdRequest = {
			stopGroupId: req.params.stopGroupId,
			stopId: req.params.stopId
		}

		try {
			await this.stopGroupService.removeStopFromGroup(request.stopGroupId, request.stopId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}
}