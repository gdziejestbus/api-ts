const Joi = require("joi");

export const createStopValidation = {
	body: {
		name: Joi.string().required(),
		coordinates: Joi.array().items(Joi.number()).required()
	}
}