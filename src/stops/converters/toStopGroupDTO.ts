import StopGroup from "../models/StopGroup";
import StopGroupDTO from "../DTO/StopGroupDTO";

export function toStopGroupDTO(stopGroup: StopGroup): StopGroupDTO {
	const StopGroupDTO: StopGroupDTO = {
		id: stopGroup.id,
		name: stopGroup.name,
		voivodeship: stopGroup.voivodeship,
		county: stopGroup.county,
		city: stopGroup.city,
		stopsId: stopGroup.stopsId,
		location: stopGroup.location,
		active: stopGroup.active
	}

	return StopGroupDTO;
}