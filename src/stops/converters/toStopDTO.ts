import Stop from "../models/Stop";
import StopDTO from "../DTO/StopDTO";

export function toStopDTO(stop: Stop): StopDTO {
	const StopDTO: StopDTO = {
		id: stop.id,
		name: stop.name,
		location: stop.location,
		active: stop.active
	}

	return StopDTO;
}