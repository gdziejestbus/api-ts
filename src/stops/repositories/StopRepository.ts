import Stop from "../models/Stop";
import IRepository from "../../shared/repository/IRepository";
import StopSchema from '../schemas/StopSchema';
import Repository from "../../shared/repository/Repository";

export interface IStopRepository extends IRepository<Stop> {
    findMany(stopsId: Array<string>): Promise<Array<Stop>>;
}

export default class StopRepository extends Repository<Stop> implements IStopRepository {
    constructor() {
        super(StopSchema);
    }

    public findMany = async (stopsId: Array<string>): Promise<Array<Stop>> => {
        const stops = await StopSchema.find({ _id: { $in: stopsId } });

        return stops.length > 0 ? stops.map(stop => stop.toObject()) : [];
    }
}