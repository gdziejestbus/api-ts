import StopGroup from "../models/StopGroup";
import IRepository from "../../shared/repository/IRepository";
import StopGroupSchema from '../schemas/StopGroupSchema';
import Repository from "../../shared/repository/Repository";

export interface IStopGroupRepository extends IRepository<StopGroup> {
	getByName(name: string): Promise<StopGroup>;
	findByNameAndCounty(name: string, county: string): Promise<StopGroup>;
	getNearestStopGroup(coords: number[], maxDistance: number): Promise<StopGroup>;
	getByBounds(coords: Array<number[]>): Promise<Array<StopGroup>>;
	findByName(text: string): Promise<Array<StopGroup>>;
}

export default class StopRepository extends Repository<StopGroup> implements IStopGroupRepository {
	constructor() {
		super(StopGroupSchema);
	}

	public getByName = async (name: string): Promise<StopGroup> => {
		const stopGroup = await StopGroupSchema.findOne({ name });

		return stopGroup ? stopGroup.toObject() : null;
	}

	public findByNameAndCounty = async (name: string, county: string): Promise<StopGroup> => {
		const stopGroup = await StopGroupSchema.findOne({ name, county });

		return stopGroup ? stopGroup.toObject() : null;
	}

	public getNearestStopGroup = async (coords: number[], maxDistance: number): Promise<StopGroup> => {
		const stopGroup = await StopGroupSchema.findOne({
			location: {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: coords
					},
					$maxDistance: maxDistance
				}
			}
		});

		return stopGroup ? stopGroup.toObject() : null;
	}

	public getByBounds = async (coords: Array<number[]>): Promise<Array<StopGroup>> => {
		if (coords.length === 2) {
			const stopGroups = await StopGroupSchema.find({
				location: {
					$geoWithin: {
						$box: coords
					}
				}
			});

			return stopGroups.length > 0 ? stopGroups.map(stopGroup => stopGroup.toObject()) : [];
		} else if (coords.length > 2) {
			const stopGroups = await StopGroupSchema.find({
				location: {
					$geoWithin: {
						$polygon: coords
					}
				}
			});

			return stopGroups.length > 0 ? stopGroups.map(stopGroup => stopGroup.toObject()) : [];
		}
	}

	public findByName = async (name: string): Promise<Array<StopGroup>> => {
		const stopGroups = await StopGroupSchema.find(this.createSearchQuery(name)).limit(10);

		return stopGroups.length > 0 ? stopGroups.map(stopGroup => stopGroup.toObject()) : [];
	}

	private createSearchQuery(name: string): Object {
		const array: string[] = this.addLatinLetters(name).split(" ");

		if (array.length === 1) return { name: { $regex: this.addLatinLetters(name), $options: "i" } };

		const searchQuery = {
			$or: []
		}

		array.forEach(text => {
			if (text.length > 0) {
				searchQuery.$or.push({ name: { $regex: text, $options: "i" } });
			}
		});

		return searchQuery;
	}

	private addLatinLetters(text): string {
		return text
			.replace('o', '[oó]').replace('O', '[OÓ]')
			.replace('a', '[aą]').replace('A', '[AĄ]')
			.replace('c', '[cć]').replace('C', '[CĆ]')
			.replace('e', '[eę]').replace('E', '[EĘ]')
			.replace('l', '[lł]').replace('L', '[LŁ]')
			.replace('n', '[nń]').replace('N', '[NŃ]')
			.replace('s', '[sś]').replace('S', '[SŚ]')
			.replace('z', '[zżź]').replace('Z', '[ZŻŹ]');
	}
}