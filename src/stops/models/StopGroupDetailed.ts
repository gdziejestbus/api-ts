import LocationData from "../../shared/models/LocationData";
import Stop from "./Stop";

export default interface StopGroupDetailed {
	readonly id?: string;
	readonly name: string;
	readonly voivodeship: string;
	readonly county: string;
	readonly city: string;
	readonly stopsId: Array<string>;
	readonly stops: Array<Stop>;
	readonly location?: LocationData;
	readonly active?: boolean;
}