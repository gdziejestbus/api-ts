import LocationData from "../../shared/models/LocationData";

export default interface CreateStop {
    readonly id?: string;
    readonly name: string;
    readonly location: LocationData;
	readonly active?: boolean;
}