export interface AddressDetailed {
	readonly name: string;
	readonly details: {
		readonly number: number | null;
		readonly premise: number | null;
		readonly street: number | null;
		readonly city: number | null;
		readonly county: number | null;
		readonly voivodeship: number | null;
		readonly placeName: number | null;
	}
	readonly location: {
		readonly type: string;
		readonly coordinates: number[];
	}
	readonly nearestStopGroupId?: string | null;
	readonly nearestStopGroupName?: string | null;
}