import LocationData from '../../shared/models/LocationData';

export default interface Stop {
    readonly id?: string;
    readonly name: string;
    readonly location: LocationData;
	readonly active?: boolean;
}