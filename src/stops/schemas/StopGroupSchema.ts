import { Document, model, Schema } from 'mongoose';
import LocationData from '../../shared/models/LocationData';

export interface IStopGroupSchema extends Document {
	readonly name: string;
	readonly voivodeship: string;
	readonly county: string;
	readonly city: string;
	readonly stopsId: Array<string>;
	readonly location: LocationData;
	readonly active: boolean;
}

const StopGroupSchema: Schema = new Schema({
	name: {
		type: String,
		required: true,
		trim: true
	},
	voivodeship: {
		type: String,
		required: true
	},
	county: {
		type: String,
		required: true
	},
	city: {
		type: String,
		required: true
	},
	stopsId: {
		type: [String],
		default: []
	},
	location: {
		type: {
			type: String,
			enum: ['Point'],
			default: 'Point',
		},
		coordinates: {
			type: [Number],
			default: [0, 0],
		}
	},
	active: {
		type: Boolean,
		default: false
	}
});

StopGroupSchema.virtual('id').get(function () {
	return this._id.toHexString();
});

StopGroupSchema.set('toObject', {
	virtuals: true
});

StopGroupSchema.index({ location: '2dsphere' });

export default model<IStopGroupSchema>("StopGroup", StopGroupSchema);