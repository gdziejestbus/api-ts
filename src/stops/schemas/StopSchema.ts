import { Document, model, Schema } from 'mongoose';
import LocationData from '../../shared/models/LocationData';

export interface IStopSchema extends Document {
	readonly name: string;
	readonly location: LocationData;
	readonly active: boolean;
}

const StopSchema: Schema = new Schema({
	name: {
		type: String,
		required: true,
		trim: true
	},
	location: {
		type: {
			type: String,
			enum: ['Point'],
			default: 'Point',
		},
		coordinates: {
			type: [Number],
			default: [0, 0],
		}
	},
	active: {
		type: Boolean,
		default: false
	}
});

StopSchema.virtual('id').get(function () {
	return this._id.toHexString();
});

StopSchema.set('toObject', {
	virtuals: true
});

StopSchema.index({ location: '2dsphere' });

export default model<IStopSchema>("Stop", StopSchema);