import StopGroupDTO from "./StopGroupDTO";

export interface GetByIdResponse {
    readonly stopGroup: StopGroupDTO
}

export interface CreateStopGroupResponse {
    readonly stopGroupId: string;
}