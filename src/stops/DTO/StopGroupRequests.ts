export interface GetByIdRequest {
    readonly stopGroupId: string;
}

export interface CreateStopGroupRequest {
    readonly name: string;
    readonly city: string;
    readonly stopsId: Array<string>;
    readonly coordinates: [number];
}

export interface UpdateStopGroupRequest {
    readonly id: string;
    readonly name: string;
    readonly city: string;
    readonly stopsId: Array<string>;
}

export interface EditStopsIdRequest {
    readonly stopGroupId: string;
    readonly stopId: string;
}