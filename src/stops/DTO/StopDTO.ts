import LocationData from "../../shared/models/LocationData";

export default interface StopDTO {
    readonly id?: string;
    readonly name: string;
	readonly location: LocationData;
	readonly active: boolean;
}