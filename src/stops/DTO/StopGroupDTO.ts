import LocationData from "../../shared/models/LocationData";

export default interface StopGroupDTO {
    readonly id: string;
    readonly name: string;
	readonly voivodeship: string;
	readonly county: string;
	readonly city: string;
    readonly stopsId: Array<string>;
    readonly location: LocationData;
	readonly active: boolean;
}