export interface GetByIdRequest {
	readonly stopId: string;
}

export interface CreateStopRequest {
	readonly name: string;
	readonly coordinates: [number];
}

export interface UpdateStopRequest {
	readonly id: string;
	readonly name: string;
	readonly city: string;
	readonly voivodeship: string;
	readonly county: string;
	readonly coordinates: [number];
}

export interface GetNearestRequest {
	readonly latitude: number;
	readonly longitude: number;
}

export interface GetByBoundsRequest {
	readonly coords: [[number]];
}