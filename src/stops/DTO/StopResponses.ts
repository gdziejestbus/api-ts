import StopDTO from "./StopDTO";
import LocationData from "../../shared/models/LocationData";
import StopGroupDetailed from "../models/StopGroupDetailed";
import StopGroup from "../models/StopGroup";

export interface GetByIdResponse {
	readonly stop: StopDTO
}

export interface CreateStopResponse {
	readonly stopId: string;
}

export interface GetNearestResponse {
	readonly location: LocationData;
	readonly stopsId: string[];
	readonly active: boolean;
	readonly id: string;
	readonly name: string;
	readonly voivodeship: string;
	readonly county: string;
	readonly stops: StopDTO[];
	readonly address: string;
}

export interface GetByBoundsResponse {
	readonly stopGroups: StopGroupDetailed[];
	readonly amount: number;
}

export interface SearchStopGroupsResponse {
	readonly stopGroups: StopGroup[];
	readonly addresses: any[];
}