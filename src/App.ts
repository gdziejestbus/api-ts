import express from 'express';
import cors from "cors";
import bodyParser from "body-parser";
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import morgan from 'morgan';
import * as errorHandler from './shared/api/ErrorHandlers';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from '../swagger.json';
import device from 'express-device';
import useragent from 'express-useragent';

import * as companiesModule from './companies/module';
import * as routesModule from './routes/module';
import * as coursesModule from './courses/module';
import * as stopModule from './stops/module';
import * as imagesModule from './images/module';
import * as userModule from './users/module';
import * as authModule from './auth/module';

class App {
	public express: express.Application;

	constructor() {
		this.express = express();
		this.setMiddleWares();
		this.setRoutes();
		this.catchErrors();
	}

	private setMiddleWares(): void {
		this.express.use(cors());
		this.express.use(morgan('dev'));
		this.express.use(bodyParser.json());
		this.express.use(cookieParser());
		this.express.use(bodyParser.urlencoded({ extended: false }));
		this.express.use(helmet());
		this.express.use(device.capture());
		this.express.use(useragent.express());
	}

	private setRoutes(): void {
		this.express.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
		this.express.use('/api/v1/companies', companiesModule.routes);
		this.express.use('/api/v1/routes', routesModule.routes);
		this.express.use('/api/v1', coursesModule.routes);
		this.express.use('/api/v1/stops', stopModule.routes);
		this.express.use('/api/v1/images', imagesModule.routes);
		this.express.use('/api/v1/users', userModule.routes);
		this.express.use('/api/v1/auth', authModule.routes);
	}

	private catchErrors(): void {
		this.express.use(errorHandler.notFound);
		this.express.use(errorHandler.internalServerError);
	}
}

export default new App().express;