import app from './App';
import config from './shared/config';
import './shared/database';

const PORT = config.PORT;

app.listen(PORT, err => {
	if (err) {
		return console.log(err);
	}

	console.log(`Server is listening on ${PORT}`);
});

declare module 'express' {
	interface Request {
		device: {
			type: string;
		};
		useragent: {
			browser: string;
		};
		user: {
			id: string;
			role: string;
		};
	}
}