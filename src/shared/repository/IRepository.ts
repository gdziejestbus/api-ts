export default interface IRepository<T> {
	create(tmodel: T): Promise<T>;
	getById(id: string): Promise<T>;
	deleteById(id: string): Promise<void>;
	update(tmodel: T): Promise<void>;
}