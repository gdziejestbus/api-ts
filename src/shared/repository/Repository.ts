import IRepository from "./IRepository";
import { Document, Model } from "mongoose";

export default class Repository<T> implements IRepository<T> {
	private _schema: Model<Document>;

	constructor(schema: Model<Document>) {
		this._schema = schema;
	}

	public async create(tmodel: any): Promise<T> {
		const createdModel = await this._schema.create(tmodel);

		const modelToReturn = { ...createdModel.toObject(), id: createdModel._id };

		return modelToReturn;
	}

	public async getById(id: string): Promise<any> {
		const model = await this._schema.findById(id);

		return model ? model.toObject() : null;
	}

	public async deleteById(id: string): Promise<void> {
		await this._schema.findByIdAndDelete(id);
	}

	public async update(tmodel: any): Promise<void> {
		const id = tmodel.id || tmodel._id;

		await this._schema.findByIdAndUpdate(id, tmodel);
	}
}