import { Response } from "express";

export const errorHandler = (res, err): Response => {
    console.log(err);

    switch (err.message) {
        case "NotFound":
            return res.status(404).json({ Error: "Resource not found" });
        case err.statusCode === 404:
            return res.status(404).json({ Error: "Resource not found" });
        case "InsufficientData":
            return res.status(400).json({ Error: "Insufficient data" });
        case "Exist":
            return res.status(409).json({ Error: "Resource already exists" });
        case "WrongCredentials":
            return res.status(403).json({ Error: "Wrong email or password" });
        default:
            return res.status(500).json({ Error: err.message });
    }
}