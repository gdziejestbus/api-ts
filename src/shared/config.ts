import * as dotenv from 'dotenv';

dotenv.config();

export default {
	APP: process.env.APP || 'development',
	PORT: process.env.PORT || '5000',

	DB_DIALECT: process.env.DB_DIALECT,
	DB_NAME: process.env.DB_NAME,
	DB_USER: process.env.DB_USER,
	DB_PASSWORD: process.env.DB_PASSWORD,

	AZURE_STORAGE_ACCOUNT_NAME: process.env.AZURE_STORAGE_ACCOUNT_NAME,
	AZURE_STORAGE_ACCOUNT_ACCESS_KEY: process.env.AZURE_STORAGE_ACCOUNT_ACCESS_KEY,
	AZURE_CONNECTION_STRING: `https://${process.env.AZURE_STORAGE_ACCOUNT_NAME}.blob.core.windows.net`,

	GOOGLE_API_KEY: process.env.GOOGLE_API_KEY,

	JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
	JWT_EXPIRE_TIME: new Date(Date.now() + 1000 * 60 * 60),

	EMAIL_REGEX: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	PASSWORD_REGEX: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/
};