export default interface AddressData {
	readonly street: string,
	readonly city: string;
	readonly zipCode: string;
	readonly country: string;
}