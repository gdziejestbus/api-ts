export default interface LocationData {
	readonly type: string;
	readonly coordinates: [number]
}