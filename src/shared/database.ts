import mongoose from 'mongoose';
import CONFIG from './config';

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.set('debug', true)

export default (async () => {
	try {
		await mongoose.connect(
			`mongodb+srv://${CONFIG.DB_USER}:${CONFIG.DB_PASSWORD}@dev-tiq81.mongodb.net/${CONFIG.DB_NAME}?retryWrites=true&w=majority`,
			{ useNewUrlParser: true }
		);
		console.log('Database successfully connected');
	} catch (err) {
		console.log(`${err} Could not connect to the database. Exiting now...`);
		process.exit();
	}
})();