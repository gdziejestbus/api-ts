import jwt from 'jsonwebtoken';
import config from '../config';
import { Response, Request } from 'express';
import AuthRequest from '../../auth/models/AuthRequest';

export const authMiddleware = (roles: string[]) => {
	return (req: Request, res: Response, next: Function) => {
		const token = req.cookies ? req.cookies.token : false;

		if (token) {
			try {
				const decoded: AuthRequest = jwt.verify(token, config.JWT_SECRET_KEY, null);

				if (roles.length !== 0 && !roles.includes(decoded.role)) {
					return res.status(401).json({ Error: "Not authorized" });
				}

				req.user = decoded;
			} catch (err) {
				console.log(err);
				return res.status(401).json({ Error: "Not authorized" });
			}
		} else {
			return res.status(401).json({ Error: "Not authorized" });
		}

		next();
	}
}