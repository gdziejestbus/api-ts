import RouteController from "./api/controller/RouteController";
import { Router } from "express";
import validate from 'express-validation';
import { getRouteByIdValidation, createRouteValidation } from "./validation/RouteRequestValidation";
import { updateTimeTableRequest, createTimeTableRequest } from "./validation/PriceTableRequestValidation";
import PriceTableController from "./api/controller/PriceTableController";

const routeController = new RouteController();
const priceTableController = new PriceTableController();

const routes: Router = Router();

// routes
routes.get('/:routeId', validate(getRouteByIdValidation), routeController.getRouteById);
routes.delete('/:routeId', routeController.deleteById);
routes.get("/stop/:stopId", routeController.getStopRoutes);
routes.post('/create', validate(createRouteValidation), routeController.createRoute);
routes.get('/company/:companyId', routeController.getCompanyRoutes);
routes.delete('/company/:companyId', routeController.deleteCompanyRoutes);

// price tables
routes.get("/pricetable/:priceTableId", priceTableController.getPriceTableById);
routes.get("/pricetable/route/:routeId", priceTableController.getPriceTableByRouteId);
routes.delete("/pricetable/:priceTableId", priceTableController.deleteById);
routes.delete("/pricetable/route/:routeId", priceTableController.deleteByRouteId);
routes.post("/pricetable/create", validate(createTimeTableRequest), priceTableController.createPriceTable);
routes.put("/pricetable/:priceTableId", validate(updateTimeTableRequest), priceTableController.updatePriceTable);

export { routes };