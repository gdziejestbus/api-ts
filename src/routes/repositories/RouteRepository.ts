import Route from "../models/Route";
import IRepository from "../../shared/repository/IRepository";
import RouteSchema from '../schemas/RouteSchema';
import Repository from "../../shared/repository/Repository";

export interface IRouteRepository extends IRepository<Route> {
	findRoutes(fromStops: Array<string>, toStops: Array<string>): Promise<Array<Route>>;
	getCompanyRoutes(companyId: string): Promise<Array<Route>>;
	deleteCompanyRoutes(companyId: string): Promise<void>;
	getStopRoutes(stopId: string): Promise<Array<Route>>;
}

export default class RouteRepository extends Repository<Route> implements IRouteRepository {
	constructor() {
		super(RouteSchema);
	}

	public async findRoutes(fromStops: Array<string>, toStops: Array<string>): Promise<Array<Route>> {
		const searchQuery = this.createSearchQuery(fromStops, toStops);

		const routes = await RouteSchema.find(searchQuery);

		return routes.length > 0 ? routes.map(route => route.toObject()) : [];
	}

	public async getCompanyRoutes(companyId: string): Promise<Array<Route>> {
		const routes = await RouteSchema.find({ companyId });

		return routes.length > 0 ? routes.map(route => route.toObject()) : [];
	}

	public async deleteCompanyRoutes(companyId: string): Promise<void> {
		await RouteSchema.deleteMany({ companyId });
	}

	public async getStopRoutes(stopId: string): Promise<Array<Route>> {
		const routes = await RouteSchema.find({ stopsId: { $all: [stopId] } });

		return routes.length > 0 ? routes.map(route => route.toObject()) : [];
	}

	private createSearchQuery(fromStops: Array<string>, toStops: Array<string>): Object {
		const searchQuery = {
			$or: []
		}

		fromStops.forEach(from => {
			toStops.forEach(to => {
				searchQuery.$or.push({ stopsId: { $all: [from, to] } });
			});
		});

		return searchQuery;
	}
}