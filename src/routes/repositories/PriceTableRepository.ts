import IRepository from "../../shared/repository/IRepository";
import Repository from "../../shared/repository/Repository";
import PriceTableSchema from '../schemas/PriceTable';
import PriceTable from "../models/PriceTable";

export interface IPriceTableRepository extends IRepository<PriceTable> {
    deleteByRouteId(routeId: string): Promise<void>;
    getByRouteId(routeId: string): Promise<PriceTable>;
    getManyByRoutesId(routesId: Array<string>): Promise<Array<PriceTable>>;
    deleteMany(routesId: Array<string>): Promise<void>;
}

export default class PriceTableRepository extends Repository<PriceTable> implements IPriceTableRepository {
    constructor() {
        super(PriceTableSchema);
    }

    public async deleteByRouteId(routeId: string): Promise<void> {
        await PriceTableSchema.deleteMany({ routeId });
    }

    public async getByRouteId(routeId: string): Promise<PriceTable> {
        const priceTable = await PriceTableSchema.findOne({ routeId });

        return priceTable ? priceTable.toJSON() : null;
    }

    public async getManyByRoutesId(routesId: Array<string>): Promise<Array<PriceTable>> {
        const priceTables = await PriceTableSchema.find({ routeId: { $in: routesId } });

        return priceTables.length > 0 ? priceTables.map(priceTable => priceTable.toObject()) : [];
    }

    public async deleteMany(routesId): Promise<void> {
        await PriceTableSchema.deleteMany({ routeId: { $in: routesId } });
    }
}