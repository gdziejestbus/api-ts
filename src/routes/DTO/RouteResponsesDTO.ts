import RouteDTO from "./RouteDTO";
import RouteDetailedDTO from "./RouteDetailedDTO";
import StopRouteDTO from "./StopRouteDTO";

export interface GetRouteByIdResponse {
    readonly route: RouteDTO
}

export interface CreateRouteResponse {
    readonly routeId: string;
}

export interface GetCompanyRoutesResponse {
    readonly routes: Array<RouteDetailedDTO>;
}

export interface GetStopRoutesResponse {
	readonly routes: StopRouteDTO[];
}