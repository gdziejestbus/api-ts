export interface GetPriceTableByIdRequest {
    priceTableId: string;
}

export interface GetPriceTableByRouteIdRequest {
    routeId: string;
}

export interface CreatePriceTableRequest {
    routeId: string;
    normal: {
        readonly [startStop: string]: {
            readonly [endStop: string]: number;
        }
    };
    halfPrice?: {
        readonly [startStop: string]: {
            readonly [endStop: string]: number;
        }
    };
}