import Stop from "../../stops/models/Stop";

export default interface RouteDetailedDTO {
    readonly id: string;
    readonly companyId: string;
    readonly stops: Array<Stop>;
    readonly name: string;
    readonly type: string;
}