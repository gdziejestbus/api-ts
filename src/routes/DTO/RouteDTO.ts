export default interface RouteDTO {
    readonly id?: string;
    readonly companyId: string;
    readonly stopsId: Array<string>;
    readonly name: string;
    readonly type: string;
}