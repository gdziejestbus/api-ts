export default interface PriceTableDTO {
    readonly id: string;
    readonly routeId: string;
    readonly normal: {
        readonly [startStop: string]: {
            readonly [endStop: string]: number;
        }
    };
    readonly halfPrice?: {
        readonly [startStop: string]: {
            readonly [endStop: string]: number;
        }
    };
}