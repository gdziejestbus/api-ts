export default interface StopRouteDTO {
    readonly routeName: string;
    readonly companyName: string;
    readonly routeId: string;
    readonly routeType: string;
}