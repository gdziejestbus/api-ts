import PriceTableDTO from "./PriceTableDTO";

export interface GetPriceTableByIdResponse {
    readonly priceTable: PriceTableDTO
}

export interface CretePriceTableResponse {
    readonly priceTableId: string
}