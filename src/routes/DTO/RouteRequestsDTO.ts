export interface GetRouteByIdRequest {
    readonly routeId: string
}

export interface CreateRouteRequest {
    readonly stopsId: Array<string>;
    readonly companyId: string;
    readonly name: string;
    readonly type: string;
}

export interface GetCompanyRoutesRequest {
    readonly companyId: string;
}