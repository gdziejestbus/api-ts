import Joi from 'joi';

export const createTimeTableRequest = {
    body: {
        routeId: Joi.string().required(),
        normal: Joi.object().required(),
        halfPrice: Joi.object()
    }
}

export const updateTimeTableRequest = {
    body: {
        normal: Joi.object().required(),
        halfPrice: Joi.object()
    },
    params: {
        priceTableId: Joi.string().required()
    }
}