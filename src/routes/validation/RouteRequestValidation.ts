const Joi = require("joi");

export const getRouteByIdValidation = {
    params: {
        routeId: Joi.string().required()
    }
}

export const createRouteValidation = {
    body: {
        companyId: Joi.string().required(),
        stopsId: Joi.array().items(Joi.string()).required(),
        name: Joi.string().required(),
        type: Joi.string()
    }
}