import PriceTable from "../models/PriceTable";
import PriceTableRepository, { IPriceTableRepository } from "../repositories/PriceTableRepository";

export interface IPriceTableService {
	getPriceTableById(id: string): Promise<PriceTable>;
	getPriceTableByRouteId(routeId: string): Promise<PriceTable>;
	getPriceTableByManyRoutesId(routesId: Array<string>): Promise<Array<PriceTable>>;
	deleteById(id: string): Promise<void>;
	deleteByRouteId(routeId: string): Promise<void>;
	deleteMany(routesId: Array<string>): Promise<void>;
	createPriceTable(priceTable: PriceTable): Promise<string>;
	updatePriceTable(priceTable: PriceTable): Promise<void>;
}

export default class PriceTableService implements IPriceTableService {
	private priceTableRepository: IPriceTableRepository;

	constructor() {
		this.priceTableRepository = new PriceTableRepository();
	}

	public getPriceTableById = async (id: string): Promise<PriceTable> => {
		const priceTable = await this.priceTableRepository.getById(id);

		if (!priceTable) throw new Error("NotFound");

		return priceTable;
	}

	public getPriceTableByRouteId = async (routeId: string): Promise<PriceTable> => {
		const priceTable = await this.priceTableRepository.getByRouteId(routeId);

		if (!priceTable) throw new Error("NotFound");

		return priceTable;
	}

	public getPriceTableByManyRoutesId = async (routesId: Array<string>): Promise<Array<PriceTable>> => {
		const priceTables = await this.priceTableRepository.getManyByRoutesId(routesId);

		return priceTables;
	}

	public deleteById = async (id: string): Promise<void> => {
		await this.priceTableRepository.deleteById(id);
	}

	public deleteByRouteId = async (routeId: string): Promise<void> => {
		await this.priceTableRepository.deleteByRouteId(routeId);
	}

	public deleteMany = async (routesId: Array<string>): Promise<void> => {
		await this.priceTableRepository.deleteMany(routesId);
	}

	public createPriceTable = async (priceTable: PriceTable): Promise<string> => {
		const createdPriceTable = await this.priceTableRepository.create(priceTable);

		return createdPriceTable.id;
	}

	public updatePriceTable = async (priceTable: PriceTable): Promise<void> => {
		await this.priceTableRepository.update(priceTable);
	}
}