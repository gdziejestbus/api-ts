import Route from "../models/Route";
import RouteRepository, { IRouteRepository } from "../repositories/RouteRepository";
import { fiterRoutesByDirection } from "./FilterService";
import RouteDetailed from "../models/RouteDetailed";
import Stop from "../../stops/models/Stop";
import StopService, { IStopService } from "../../stops/services/StopService";
import PriceTableService, { IPriceTableService } from "./PriceTableService";
import CompanyService, { ICompanyService } from "../../companies/services/CompanyService";
import StopRoute from "../models/StopRoutes";

export interface IRouteService {
	getRouteById(routeId: string): Promise<Route>;
	createRoute(route: Route): Promise<string>;
	findRoutes(fromStops: Array<string>, toStops: Array<string>): Promise<Array<Route>>;
	getCompanyRoutes(companyId: string): Promise<Array<RouteDetailed>>;
	deleteCompanyRoutes(companyId: string): Promise<void>;
	deleteById(routeId: string): Promise<void>;
	getStopRoutes(stopId: string): Promise<Array<StopRoute>>;
}

export default class RouteService implements IRouteService {
	private routeRepository: IRouteRepository;
	private stopService: IStopService;
	private priceTableService: IPriceTableService;
	private companyService: ICompanyService;

	constructor() {
		this.routeRepository = new RouteRepository();
		this.stopService = new StopService();
		this.priceTableService = new PriceTableService();
		this.companyService = new CompanyService();
	}

	public async getRouteById(routeId: string): Promise<Route> {
		const route = await this.routeRepository.getById(routeId);

		if (!route) throw new Error("NotFound");

		return route;
	}

	public async deleteById(id: string): Promise<void> {
		await this.routeRepository.deleteById(id);

		await this.priceTableService.deleteByRouteId(id);
	}

	public async createRoute(route: Route): Promise<string> {
		const createdCompany = await this.routeRepository.create(route);

		return createdCompany.id;
	}

	public async findRoutes(fromStops: Array<string>, toStops: Array<string>): Promise<Array<Route>> {
		const routes = await this.routeRepository.findRoutes(fromStops, toStops);

		const correctDirectionRoutes = fiterRoutesByDirection(routes, fromStops, toStops);

		if (correctDirectionRoutes.length === 0) throw new Error("NotFound");

		return correctDirectionRoutes;
	}

	public async getCompanyRoutes(companyId: string): Promise<Array<RouteDetailed>> {
		const routes = await this.routeRepository.getCompanyRoutes(companyId);

		const stops = await this.getStopsFromRoutes(routes);

		const detailedRoutes = this.assignStopsToRoutes(routes, stops);

		if (detailedRoutes.length === 0) throw new Error("NotFound");

		return detailedRoutes;
	}

	public async deleteCompanyRoutes(companyId: string): Promise<void> {
		const companyRoutes = await this.routeRepository.getCompanyRoutes(companyId);

		const routesPriceTablesId: Array<string> = [];

		companyRoutes.forEach(route => {
			routesPriceTablesId.push(route.id);
		});

		await this.routeRepository.deleteCompanyRoutes(companyId);
		await this.priceTableService.deleteMany(routesPriceTablesId);
	}

	public async getStopRoutes(stopId: string): Promise<Array<StopRoute>> {
		const routes = await this.routeRepository.getStopRoutes(stopId);

		const companiesId: Array<string> = [];

		routes.forEach(route => {
			if (!companiesId.includes(route.companyId)) {
				companiesId.push(route.companyId);
			}
		});

		const companies = await this.companyService.getMany(companiesId);

		const stopRoutes: Array<StopRoute> = routes.map(route => {
			const company = companies.find(company => company.id === route.companyId);

			return {
				routeName: route.name,
				routeType: route.type,
				companyName: company.name,
				companyId: company.id,
				routeId: route.id,
				stopsId: route.stopsId
			}
		});

		return stopRoutes;
	}

	private assignStopsToRoutes(routes: Array<Route>, stops: Array<Stop>): Array<RouteDetailed> {
		const detailedRoutes = routes.map(route => {
			const routeStops: Array<Stop> = [];

			route.stopsId.map(stopId => {
				routeStops.push(stops.find(x => x.id === stopId));
			});

			const detailedRoute: RouteDetailed = {
				...route,
				stops: routeStops
			}

			return detailedRoute;
		});

		return detailedRoutes;
	}

	private async getStopsFromRoutes(routes: Array<Route>): Promise<Array<Stop>> {
		const stopsId: Array<string> = [];

		routes.map(route => {
			for (const stopId of route.stopsId) {
				if (!stopsId.includes(stopId)) {
					stopsId.push(stopId);
				}
			}
		});

		const stops = await this.stopService.getMany(stopsId);

		return stops;
	}
}