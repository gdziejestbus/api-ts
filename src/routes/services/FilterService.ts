import Route from "../models/Route";

export function fiterRoutesByDirection(routes: Array<Route>, fromStops: Array<string>, toStops: Array<string>) {
    const correctDirectionRoutes = [];

    for (const route of routes) {
        fromStops.forEach(from => {
            toStops.forEach(to => {
                if (route.stopsId.includes(from) && route.stopsId.includes(to) && route.stopsId.indexOf(from) < route.stopsId.indexOf(to)) {
                    correctDirectionRoutes.push(route);
                }
            });
        });
    }

    return correctDirectionRoutes;
}