import Route from "../models/Route";
import RouteDTO from "../DTO/RouteDTO";

export function toRouteDTO(route: Route): RouteDTO {
    const routeDTO: RouteDTO = {
        id: route.id,
        stopsId: route.stopsId,
        companyId: route.companyId,
        name: route.name,
        type: route.type
    }
    return routeDTO;
}