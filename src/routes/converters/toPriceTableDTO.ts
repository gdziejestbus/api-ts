import PriceTableDTO from "../DTO/PriceTableDTO";
import PriceTable from "../models/PriceTable";
import deepCopy from 'deep-copy';

export function toPriceTableDTO(priceTable: PriceTable): PriceTableDTO {
    const priceTableDTO: PriceTableDTO = {
        id: priceTable.id,
        routeId: priceTable.routeId,
        normal: deepCopy(priceTable.normal),
        halfPrice: deepCopy(priceTable.halfPrice)
    }

    return priceTableDTO;
}