import RouteDetailed from "../models/RouteDetailed";
import RouteDetailedDTO from "../DTO/RouteDetailedDTO";

export function toRouteDetailedDTO(routeDetailed: RouteDetailed): RouteDetailedDTO {
    const routeDetailedDTO: RouteDetailedDTO = {
        id: routeDetailed.id,
        companyId: routeDetailed.companyId,
        stops: routeDetailed.stops,
        name: routeDetailed.name,
        type: routeDetailed.type
    }

    return routeDetailedDTO;
}