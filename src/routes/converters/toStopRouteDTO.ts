import StopRoute from "../models/StopRoutes";
import StopRouteDTO from "../DTO/StopRouteDTO";

export function toStopRouteDTO(stopRoute: StopRoute): StopRouteDTO {
    const StopDTO: StopRouteDTO = {
        routeId: stopRoute.routeId,
        routeName: stopRoute.routeName,
        companyName: stopRoute.companyName,
        routeType: stopRoute.routeType
    }

    return StopDTO;
}