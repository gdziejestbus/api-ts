import { Response, Request } from "express";
import Route from '../../models/Route';
import RouteService, { IRouteService } from "../../services/RouteService";
import { errorHandler } from "../../../shared/errors/errorHandler";
import { CreateRouteResponse, GetRouteByIdResponse, GetCompanyRoutesResponse, GetStopRoutesResponse } from "../../DTO/RouteResponsesDTO";
import { CreateRouteRequest, GetRouteByIdRequest, GetCompanyRoutesRequest } from "../../DTO/RouteRequestsDTO";
import { toRouteDTO } from "../../converters/toRouteDTO";
import { toRouteDetailedDTO } from "../../converters/toRouteDetailedDTO";
import { toStopDTO } from "../../../stops/converters/toStopDTO";
import { toStopRouteDTO } from "../../converters/toStopRouteDTO";

export interface IRouteController {
	getRouteById(req: Request, res: Response): Promise<Response>;
	createRoute(req: Request, res: Response): Promise<Response>;
	getCompanyRoutes(req: Request, res: Response): Promise<Response>;
	deleteById(req: Request, res: Response): Promise<Response>;
	getStopRoutes(req: Request, res: Response): Promise<Response>;
}

export default class RouteController implements IRouteController {
	private routeService: IRouteService;

	constructor() {
		this.routeService = new RouteService();
	}

	public deleteById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetRouteByIdRequest = { ...req.params };

		try {
			await this.routeService.deleteById(request.routeId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getRouteById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetRouteByIdRequest = { routeId: req.params.routeId };

		try {
			const route = await this.routeService.getRouteById(request.routeId);

			const routeDTO = toRouteDTO(route);

			const response: GetRouteByIdResponse = {
				route: routeDTO
			}

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public createRoute = async (req: Request, res: Response): Promise<Response> => {
		const request: CreateRouteRequest = { ...req.body };

		const route: Route = { ...request };

		try {
			const routeId = await this.routeService.createRoute(route);

			const response: CreateRouteResponse = {
				routeId
			}

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getCompanyRoutes = async (req: Request, res: Response): Promise<Response> => {
		const request: GetCompanyRoutesRequest = { ...req.params };

		try {
			const routes = await this.routeService.getCompanyRoutes(request.companyId);

			const routesDTO = routes.map(route => {
				const routeWithStopsDTO = {
					...route,
					stops: route.stops.map(stop => toStopDTO(stop))
				}

				return toRouteDetailedDTO(routeWithStopsDTO);
			});

			const response: GetCompanyRoutesResponse = {
				routes: routesDTO
			};

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public deleteCompanyRoutes = async (req: Request, res: Response): Promise<Response> => {
		const request: GetCompanyRoutesRequest = { ...req.params };

		try {
			await this.routeService.deleteCompanyRoutes(request.companyId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getStopRoutes = async (req: Request, res: Response): Promise<Response> => {
		try {
			const stopRoutes = await this.routeService.getStopRoutes(req.params.stopId);

			const stopRoutesDTO = stopRoutes.map(stopRoute => toStopRouteDTO(stopRoute));

			const response: GetStopRoutesResponse = {
				routes: stopRoutesDTO
			}

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}
}