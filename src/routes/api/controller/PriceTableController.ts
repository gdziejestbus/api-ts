import { Response, Request } from "express";
import PriceTableService, { IPriceTableService } from "../../services/PriceTableService";
import { GetPriceTableByIdRequest, GetPriceTableByRouteIdRequest, CreatePriceTableRequest } from "../../DTO/PriceTableRequestsDTO";
import { toPriceTableDTO } from "../../converters/toPriceTableDTO";
import { GetPriceTableByIdResponse, CretePriceTableResponse } from "../../DTO/PriceTableResponsesDTO";
import { errorHandler } from "../../../shared/errors/errorHandler";
import PriceTable from "../../models/PriceTable";

export interface IPriceTableController {
    getPriceTableById(req: Request, res: Response): Promise<Response>;
    getPriceTableByRouteId(req: Request, res: Response): Promise<Response>;
    deleteById(req: Request, res: Response): Promise<Response>;
    deleteByRouteId(req: Request, res: Response): Promise<Response>;
    createPriceTable(req: Request, res: Response): Promise<Response>;
    updatePriceTable(req: Request, res: Response): Promise<Response>;
}

export default class PriceTableController implements IPriceTableController {
    private priceTableService: IPriceTableService;

    constructor() {
        this.priceTableService = new PriceTableService();
    }

    public getPriceTableById = async (req: Request, res: Response): Promise<Response> => {
        const request: GetPriceTableByIdRequest = { ...req.params };

        try {
            const priceTable = await this.priceTableService.getPriceTableById(request.priceTableId);

            const priceTableDTO = toPriceTableDTO(priceTable);

            const response: GetPriceTableByIdResponse = {
                priceTable: priceTableDTO
            }

            return res.status(200).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public getPriceTableByRouteId = async (req: Request, res: Response): Promise<Response> => {
        const request: GetPriceTableByRouteIdRequest = { ...req.params };

        try {
            const priceTable = await this.priceTableService.getPriceTableByRouteId(request.routeId);

            const priceTableDTO = toPriceTableDTO(priceTable);

            const response: GetPriceTableByIdResponse = {
                priceTable: priceTableDTO
            }

            return res.status(200).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public deleteById = async (req: Request, res: Response): Promise<Response> => {
        const request: GetPriceTableByIdRequest = { ...req.params };

        try {
            await this.priceTableService.deleteById(request.priceTableId);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public deleteByRouteId = async (req: Request, res: Response): Promise<Response> => {
        const request: GetPriceTableByRouteIdRequest = { ...req.params };

        try {
            await this.priceTableService.deleteByRouteId(request.routeId);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public createPriceTable = async (req: Request, res: Response): Promise<Response> => {
        const request: CreatePriceTableRequest = { ...req.body };

        const priceTable: PriceTable = { ...request };

        try {
            const priceTableId = await this.priceTableService.createPriceTable(priceTable);

            const response: CretePriceTableResponse = {
                priceTableId
            }

            return res.status(200).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public updatePriceTable = async (req: Request, res: Response): Promise<Response> => {
        const request: CreatePriceTableRequest = { ...req.body };

        const priceTable: PriceTable = { ...request, id: req.params.priceTableId };

        try {
            await this.priceTableService.updatePriceTable(priceTable);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }
}