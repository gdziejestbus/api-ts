import { Document, model, Schema } from 'mongoose';

export interface IPriceTable extends Document {
    readonly routeId: string;
    readonly normal: {
        readonly [startStop: string]: {
            readonly [endStop: string]: number;
        }
    };
    readonly halfPrice?: {
        readonly [startStop: string]: {
            readonly [endStop: string]: number;
        }
    };
}

const PriceTableSchema: Schema = new Schema({
    routeId: {
        type: String,
        required: true
    },
    normal: {
        type: Object,
        required: true
    },
    halfPrice: {
        type: Object
    }
});

PriceTableSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

PriceTableSchema.set('toObject', {
    virtuals: true
});

export default model<IPriceTable>("PriceTable", PriceTableSchema);