import { Document, model, Schema } from 'mongoose';
import { LatLng } from '../../shared/models/LatLng';

export interface IRouteSchema extends Document {
    readonly companyId: string;
    readonly stopsId: Array<string>;
    readonly type: string;
    readonly name: string;
    readonly coordinares?: Array<Array<LatLng>>;
}

const RouteSchema: Schema = new Schema({
    companyId: {
        type: String,
        required: true,
        trim: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    type: {
        type: String,
        default: "bus",
        trim: true
    },
    stopsId: {
        type: [String],
        required: true,
        trim: true
    },
    coordinates: {
        type: [[Object]]
    }
});

RouteSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

RouteSchema.set('toObject', {
    virtuals: true
});

export default model<IRouteSchema>("Route", RouteSchema);