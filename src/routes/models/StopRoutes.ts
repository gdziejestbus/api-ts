export default interface StopRoute {
    readonly routeName: string;
    readonly companyName: string;
    readonly routeId: string;
    readonly routeType: string;
    readonly companyId: string;
    readonly stopsId: string[];
}