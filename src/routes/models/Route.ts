import { LatLng } from "../../shared/models/LatLng";

export default interface Route {
    readonly id?: string;
    readonly companyId: string;
    readonly stopsId: Array<string>;
    readonly type: string;
    readonly name: string;
    readonly coordinates?: Array<Array<LatLng>>;
}