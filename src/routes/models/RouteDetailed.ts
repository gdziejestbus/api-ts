import Stop from "../../stops/models/Stop";

export default interface RouteDetailed {
    readonly id?: string;
    readonly companyId: string;
    readonly stopsId: Array<string>;
    readonly stops: Array<Stop>;
    readonly name: string;
    readonly type: string;
}