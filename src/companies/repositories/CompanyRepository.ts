import CompanySchema from '../schemas/CompanySchema';
import Company from "../models/Company";
import IRepository from "../../shared/repository/IRepository";
import { PaginateResult } from 'mongoose';
import Repository from '../../shared/repository/Repository';

export interface ICompanyRepository extends IRepository<Company> {
	findMany(companiesId: Array<string>): Promise<Array<Company>>;
	getByEmail(email: string): Promise<Company>;
	getByCity(city: string, page: number, limit: number): Promise<PaginateResult<Company>>;
	getByIdentificator(identificator: string): Promise<Company>;
	getRandom(amount: number): Promise<Array<Company>>;
	searchCompanies(text: string, page: number, limit: number): Promise<PaginateResult<Company>>;
}

export default class CompanyRepository extends Repository<Company> implements ICompanyRepository {
	constructor() {
		super(CompanySchema);
	}

	public findMany = async (companiesId: Array<string>): Promise<Array<Company>> => {
		const companies = await CompanySchema.find({ _id: { $in: companiesId } });

		return companies.length > 0 ? companies.map(company => company.toObject()) : [];
	}

	public getByEmail = async (email: string): Promise<Company> => {
		const company = await CompanySchema.findOne({ email });

		return company ? company.toObject() : null;
	}

	public getByCity = async (city: string, page: number, limit: number): Promise<PaginateResult<Company>> => {
		const companies = await CompanySchema.paginate({ "address.city": { $regex: new RegExp(city, "i") } }, { page, limit });

		return companies;
	}

	public getByIdentificator = async (identificator: string): Promise<Company> => {
		const company = await CompanySchema.findOne({ identificator });

		return company ? company.toObject() : null;
	}

	public getRandom = async (amount: number): Promise<Array<Company>> => {
		const companies = await CompanySchema.aggregate([{ $sample: { size: amount } }]);

		return companies.map(company => {
			return {
				...company,
				id: company._id
			}
		});
	}

	public searchCompanies = async (text: string, page: number, limit: number): Promise<PaginateResult<Company>> => {
		return await CompanySchema.paginate({
			$or: [
				{ name: { $regex: text, $options: "i" } },
				{ "address.city": { $regex: text, $options: "i" } }
			]
		}, { page, limit });
	}
}