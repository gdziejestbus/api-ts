import IRepository from "../../shared/repository/IRepository";
import Bus from "../models/Bus";
import BusSchema from "../schemas/BusSchema";
import Repository from "../../shared/repository/Repository";

export interface IBusRepository extends IRepository<Bus> {
	getByCompanyId(id: string): Promise<Array<Bus>>;
}

export default class BusRepository extends Repository<Bus> implements IBusRepository {
	constructor() {
		super(BusSchema);
	}

	public async getByCompanyId(id: string): Promise<Array<Bus>> {
		const buses = await BusSchema.find({ companyId: id });

		return buses.length > 0 ? buses.map(bus => bus.toObject()) : [];
	}
}