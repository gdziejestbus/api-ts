import Company from "../models/Company";
import CompanyDTO from "../DTO/CompanyDTO";

export default function toCompanyDTO(company: Company): CompanyDTO {
    const companyDTO: CompanyDTO = {
        id: company.id,
        name: company.name,
        address: company.address,
        phone: company.phone,
        email: company.email,
        description: company.description,
        businessPlan: company.businessPlan,
        active: company.active,
        identificator: company.identificator,
        logoPath: company.logoPath
    }

    return companyDTO;
}