import BusDTO from "../DTO/BusDTO";
import Bus from "../models/Bus";

export default function toBusDTO(bus: Bus): BusDTO {
    const busDTO: BusDTO = {
        id: bus.id,
        name: bus.name,
        companyId: bus.companyId,
        placesAmount: bus.placesAmount,
        vehicleModel: bus.vehicleModel
    }

    return busDTO;
}