import BusDTO from "./BusDTO";

export interface CreateBusResponse {
    readonly busId: string;
}

export interface GetBusByIdResponse {
    readonly bus: BusDTO;
}