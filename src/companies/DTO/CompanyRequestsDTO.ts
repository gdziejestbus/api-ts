import AddressData from "../../shared/models/AddressData";

export interface GetCompanyByIdRequest {
    readonly id: string;
}

export interface CreateCompanyRequest {
    readonly name: string;
    readonly address: AddressData;
    readonly phone: string;
    readonly email: string;
}

export interface UpdateCompanyContactRequest {
    readonly address: AddressData;
    readonly phone: string;
    readonly email: string;
    readonly id: string;
}

export interface UpdateCompanyInfoRequest {
    readonly name: string;
    readonly description: string;
    readonly webPage: string;
    readonly id: string;
}

export interface UpdateCompanyLogoRequest {
    readonly companyId: string;
    readonly logoPath: string;
}

export interface GetRandomCompaniesRequest {
    readonly amount: number;
}

export interface SearchOptionsRequest {
    readonly amount: number;
    readonly page: number;
    readonly text: string;
}

export interface PaginateOptionsRequest {
    readonly amount: number;
    readonly page: number;
}