export interface CreateBusRequest {
    readonly name: string;
    readonly companyId: string;
    readonly vehicleModel: string;
    readonly placesAmount: number;
}

export interface UpdateBusRequest {
    readonly id: string;
    readonly name: string;
    readonly vehicleModel: string;
    readonly placesAmount: number;
}