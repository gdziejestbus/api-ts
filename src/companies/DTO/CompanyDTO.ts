import AddressData from '../../shared/models/AddressData';

export default interface CompanyDTO {
	readonly id: string;
	readonly name: string;
	readonly address: AddressData;
	readonly logoPath?: string;
	readonly webPage?: string;
	readonly phone: string;
	readonly email: string;
	readonly description: string;
	readonly businessPlan: string;
	readonly active: boolean;
	readonly identificator: string;
}