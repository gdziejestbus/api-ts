import CompanyDTO from "./CompanyDTO";
import Company from "../models/Company";

export interface CreateCompanyResponse {
    readonly companyId: string;
}

export interface GetCompanyByIdResponse {
    readonly company: CompanyDTO;
}

export interface GetRandomCompaniesResponse {
    readonly companies: CompanyDTO[];
}

export interface PaginateResponse {
    readonly totalDocs: number | Company[];
    readonly hasPrevPage: number | Company[];
    readonly hasNextPage: number | Company[];
    readonly page: number;
    readonly totalPages: number | Company[];
    readonly prevPage: number | Company[];
    readonly nextPage: number | Company[];
    readonly companies: CompanyDTO[];
}