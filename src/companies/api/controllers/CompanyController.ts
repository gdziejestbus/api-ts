import CompanyService from '../../services/CompanyService';
import Company from '../../models/Company';
import { ICompanyService } from '../../services/CompanyService';
import { Request, Response } from 'express';
import { errorHandler } from '../../../shared/errors/errorHandler';
import toCompanyDTO from '../../converters/toCompanyDTO';
import { GetCompanyByIdRequest, CreateCompanyRequest, UpdateCompanyContactRequest, UpdateCompanyInfoRequest, GetRandomCompaniesRequest, SearchOptionsRequest, PaginateOptionsRequest, UpdateCompanyLogoRequest } from '../../DTO/CompanyRequestsDTO';
import { GetCompanyByIdResponse, CreateCompanyResponse, GetRandomCompaniesResponse, PaginateResponse } from '../../DTO/CompanyResponsesDTO';
import CompanyContactData from '../../models/CompanyContactData';
import CompanyInfoData from '../../models/CompanyInfoData';

export interface ICompanyController {
	createCompany(req: Request, res: Response): Promise<Response>;
	getCompanyById(req: Request, res: Response): Promise<Response>;
	updateContactData(req: Request, res: Response): Promise<Response>;
	updateInfoData(req: Request, res: Response): Promise<Response>;
	deleteById(req: Request, res: Response): Promise<Response>;
	getCompaniesByCity(req: Request, res: Response): Promise<Response>;
}

export default class CompanyController implements ICompanyController {
	private companyService: ICompanyService;

	constructor() {
		this.companyService = new CompanyService();
	}

	public getCompanyById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetCompanyByIdRequest = {
			id: req.params.companyId
		}

		try {
			const company = await this.companyService.getCompanyById(request.id);

			const response: GetCompanyByIdResponse = {
				company: toCompanyDTO(company)
			}

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getCompanyByIdentificator = async (req: Request, res: Response): Promise<Response> => {
		try {
			const company = await this.companyService.getCompanyByIdentificator(req.params.identificator);

			const companyDTO = toCompanyDTO(company);

			const response: GetCompanyByIdResponse = {
				company: companyDTO
			}

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getCompaniesByCity = async (req: Request, res: Response): Promise<Response> => {
		const paginateOptions: PaginateOptionsRequest = {
			amount: req.query.amount || 10,
			page: req.query.page || 1
		}

		try {
			const companies = await this.companyService.getByCity(req.params.city, paginateOptions.page, paginateOptions.amount);

			const companiesResponse: PaginateResponse = {
				totalDocs: companies.totalDocs,
				hasPrevPage: companies.hasPrevPage,
				hasNextPage: companies.hasNextPage,
				page: companies.page,
				totalPages: companies.totalPages,
				prevPage: companies.prevPage,
				nextPage: companies.nextPage,
				companies: companies.docs.map(company => toCompanyDTO(company))
			}

			return res.status(200).json(companiesResponse);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public createCompany = async (req: Request, res: Response): Promise<Response> => {
		const request: CreateCompanyRequest = { ...req.body };

		const company: Company = { ...request };

		try {
			const companyId = await this.companyService.createCompany(company);

			const response: CreateCompanyResponse = {
				companyId
			}

			return res.status(201).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public updateContactData = async (req: Request, res: Response): Promise<Response> => {
		const request: UpdateCompanyContactRequest = { ...req.body, id: req.params.companyId };

		const companyContact: CompanyContactData = request;

		try {
			await this.companyService.updateContact(companyContact);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public updateInfoData = async (req: Request, res: Response): Promise<Response> => {
		const request: UpdateCompanyInfoRequest = { ...req.body, id: req.params.companyId };

		const companyInfoData: CompanyInfoData = request;

		try {
			await this.companyService.updateInfo(companyInfoData);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public updateLogo = async (req: Request, res: Response): Promise<Response> => {
		const request: UpdateCompanyLogoRequest = { logoPath: req.body.logoPath, companyId: req.params.companyId };

		try {
			await this.companyService.updateLogo(request.companyId, request.logoPath);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public deleteById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetCompanyByIdRequest = { id: req.params.companyId };

		try {
			await this.companyService.deleteById(request.id);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getRandomCompanies = async (req: Request, res: Response): Promise<Response> => {
		const request: GetRandomCompaniesRequest = { amount: parseInt(req.query.amount) || 10 };

		try {
			const companies = await this.companyService.getRandomCompanies(request.amount);

			const response: GetRandomCompaniesResponse = {
				companies: companies.map(company => toCompanyDTO(company))
			}

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public searchCompanies = async (req: Request, res: Response): Promise<Response> => {
		const serachOptions: SearchOptionsRequest = {
			amount: req.query.amount || 10,
			page: req.query.page || 1,
			text: req.query.text || ""
		}

		try {
			const companies = await this.companyService.searchCompanies(serachOptions.text, serachOptions.page, serachOptions.amount);

			const companiesResponse: PaginateResponse = {
				totalDocs: companies.totalDocs,
				hasPrevPage: companies.hasPrevPage,
				hasNextPage: companies.hasNextPage,
				page: companies.page,
				totalPages: companies.totalPages,
				prevPage: companies.prevPage,
				nextPage: companies.nextPage,
				companies: companies.docs.map(company => toCompanyDTO(company))
			}

			return res.status(200).json(companiesResponse);
		} catch (err) {
			return errorHandler(res, err);
		}
	}
}