import BusService, { IBusService } from "../../services/BusService";
import { errorHandler } from "../../../shared/errors/errorHandler";
import { Request, Response } from "express";
import toBusDTO from "../../converters/toBusDTO";
import { CreateBusRequest, UpdateBusRequest } from "../../DTO/BusRequestsDTO";
import Bus from "../../models/Bus";
import { CreateBusResponse, GetBusByIdResponse } from "../../DTO/BusResponsesDTO";

export interface IBusController {
	createBus(req: Request, res: Response): Promise<Response>;
	getById(req: Request, res: Response): Promise<Response>;
	deleteById(req: Request, res: Response): Promise<Response>;
	update(req: Request, res: Response): Promise<Response>;
	getByCompanyId(req: Request, res: Response): Promise<Response>;
}

export default class BusController implements IBusController {
	private busService: IBusService;

	constructor() {
		this.busService = new BusService();
	}

	public createBus = async (req: Request, res: Response): Promise<Response> => {
		const request: CreateBusRequest = { ...req.body };

		const bus: Bus = { ...request };

		try {
			const busId = await this.busService.createBus(bus);

			const response: CreateBusResponse = {
				busId
			}

			return res.status(201).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getById = async (req: Request, res: Response): Promise<Response> => {
		try {
			const bus = await this.busService.getById(req.params.busId);

			const response: GetBusByIdResponse = {
				bus: toBusDTO(bus)
			}

			res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public deleteById = async (req: Request, res: Response): Promise<Response> => {
		try {
			await this.busService.deleteById(req.params.busId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public update = async (req: Request, res: Response): Promise<Response> => {
		const bus: UpdateBusRequest = { ...req.body, id: req.params.busId };

		try {
			await this.busService.update(bus);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getByCompanyId = async (req: Request, res: Response): Promise<Response> => {
		try {
			const buses = await this.busService.getByCompanyId(req.params.companyId);

			return res.status(200).json({ buses });
		} catch (err) {
			return errorHandler(res, err);
		}
	}
}