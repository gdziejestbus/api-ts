import Company from "../models/Company";

export const generateIdentificator = (company: Company) => {
    const address = company.address.city.toLowerCase()
        .replace("-", "_")
        .replace(" ", "_")
        .replace(/ /gi, "");

    const companyName = company.name.toLowerCase()
        .replace("-", "_")
        .replace(" ", "_")
        .replace(/ /gi, "");

    return `${address}-${companyName}`;
}