export default interface BusInfo {
	readonly id: string;
	readonly vehicleModel: string;
	readonly placesAmount: number;
	readonly name?: string;
}