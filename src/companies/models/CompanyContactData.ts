import AddressData from '../../shared/models/AddressData';

export default interface CompanyContactData {
	readonly id: string;
	readonly address: AddressData;
	readonly phone: string;
	readonly email: string;
}