export default interface CompanyInfoData {
	readonly id: string;
	readonly name: string;
	readonly description: string;
	readonly webPage: string;
}