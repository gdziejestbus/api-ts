export default interface Bus {
	readonly id?: string;
	readonly companyId: string;
	readonly vehicleModel: string;
	readonly placesAmount: number;
	readonly name?: string;
}