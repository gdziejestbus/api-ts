import Bus from "../models/Bus";
import BusRepository, { IBusRepository } from "../repositories/BusRepository";
import BusInfo from "../models/BusInfo";

export interface IBusService {
	createBus(bus: Bus): Promise<string>;
	getById(id: string): Promise<Bus>;
	deleteById(id: string): Promise<void>;
	update(bus: BusInfo): Promise<void>;
	getByCompanyId(id: string): Promise<Array<Bus>>;
}

export default class BusService implements IBusService {
	private busRepository: IBusRepository;

	constructor() {
		this.busRepository = new BusRepository();
	}

	public async createBus(bus: Bus): Promise<string> {
		const createdBus = await this.busRepository.create(bus);

		return createdBus.id;
	}

	public async getById(id: string): Promise<Bus> {
		const bus = await this.busRepository.getById(id);

		if (!bus) throw new Error("NotFound");

		return bus;
	}

	public async deleteById(id: string): Promise<void> {
		await this.busRepository.deleteById(id);
	}

	public async update(busInfo: BusInfo): Promise<void> {
		const bus = await this.busRepository.getById(busInfo.id);

		if (!bus) throw new Error("NotFound");

		const updatedBus = { ...bus, ...busInfo };

		await this.busRepository.update(updatedBus);
	}

	public async getByCompanyId(id: string): Promise<Array<Bus>> {
		const buses = await this.busRepository.getByCompanyId(id);

		if (buses.length === 0) throw new Error("NotFound");

		return buses;
	}
}