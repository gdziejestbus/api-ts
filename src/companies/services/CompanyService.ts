import Company from '../models/Company';
import CompanyRepository from '../repositories/CompanyRepository';
import { ICompanyRepository } from '../repositories/CompanyRepository';
import { PaginateResult } from 'mongoose';
import CompanyContactData from '../models/CompanyContactData';
import CompanyInfoData from '../models/CompanyInfoData';
import deepEqual from 'deep-equal';
import CourseService, { ICourseService } from '../../courses/services/CourseService';
import { generateIdentificator } from '../helpers/helpers';

export interface ICompanyService {
	createCompany(company: Company): Promise<string>;
	getCompanyById(companyId: string): Promise<Company>;
	getMany(companiesId: Array<string>): Promise<Array<Company>>;
	deleteById(id: string): Promise<void>;
	updateContact(companyContactData: CompanyContactData): Promise<boolean>;
	updateInfo(companyInfoData: CompanyInfoData): Promise<boolean>;
	updateLogo(companyId: string, logoPath: string): Promise<boolean>;
	getByCity(city: string, page: number, limit: number): Promise<PaginateResult<Company>>;
	getCompanyByIdentificator(identificator: string): Promise<Company>;
	getRandomCompanies(amount: number): Promise<Array<Company>>;
	searchCompanies(text: string, page: number, limit: number): Promise<PaginateResult<Company>>;
}

export default class CompanyService implements ICompanyService {
	private companyRepository: ICompanyRepository;

	constructor() {
		this.companyRepository = new CompanyRepository();
	}

	public async createCompany(company: Company): Promise<string> {
		const existingCompany = await this.companyRepository.getByEmail(company.email);

		if (existingCompany) throw new Error("Company with this email already exists");

		const companyData: Company = {
			...company,
			identificator: generateIdentificator(company)
		}

		const createdCompany = await this.companyRepository.create(companyData);

		return createdCompany.id;
	}

	public async getCompanyById(companyId: string): Promise<Company> {
		const company = await this.companyRepository.getById(companyId);

		if (!company) throw new Error("NotFound");

		return company;
	}

	public async getCompanyByIdentificator(identificator: string): Promise<Company> {
		const company = await this.companyRepository.getByIdentificator(identificator);

		if (!company) throw new Error("NotFound");

		return company;
	}

	public async getMany(companiesId: Array<string>): Promise<Array<Company>> {
		const companies = await this.companyRepository.findMany(companiesId);

		if (companies.length === 0) throw new Error("NotFound");

		return companies;
	}

	public async deleteById(id: string): Promise<void> {
		await this.companyRepository.deleteById(id);

		// TO DO:
		// remove all company buses, routes, courses, calendars
	}

	public async updateInfo(companyInfoData: CompanyInfoData): Promise<boolean> {
		const company = await this.companyRepository.getById(companyInfoData.id);

		if (!company) return false;

		const updatedCompany: Company = { ...company, ...companyInfoData };

		const companyWithIdentificator: Company = { ...updatedCompany, identificator: generateIdentificator(updatedCompany) };

		await this.companyRepository.update(companyWithIdentificator);

		return true;
	}

	public async updateLogo(companyId: string, logoPath: string): Promise<boolean> {
		if (!logoPath) throw new Error("InsufficientData");

		const company = await this.companyRepository.getById(companyId);

		if (!company) return false;

		const updatedCompany: Company = { ...company, logoPath };

		await this.companyRepository.update(updatedCompany);

		return true;
	}

	public async updateContact(companyContactData: CompanyContactData): Promise<boolean> {
		const companyByEmail = await this.companyRepository.getByEmail(companyContactData.email);

		const company = await this.companyRepository.getById(companyContactData.id);

		if (!company) throw new Error("NotFound");

		if (companyByEmail && !deepEqual(company, companyByEmail)) throw new Error("Provided email is already taken");

		const updatedCompany: Company = { ...company, ...companyContactData };

		const companyWithIdentificator: Company = { ...updatedCompany, identificator: generateIdentificator(updatedCompany) };

		await this.companyRepository.update(companyWithIdentificator);

		return true;
	}

	public async getByCity(city: string, page: number, limit: number): Promise<PaginateResult<Company>> {
		return await this.companyRepository.getByCity(city, page, limit);
	}

	public async getRandomCompanies(amount: number): Promise<Array<Company>> {
		return await this.companyRepository.getRandom(amount);
	}

	public async searchCompanies(text: string, page: number, limit: number): Promise<PaginateResult<Company>> {
		return await this.companyRepository.searchCompanies(text, page, limit);
	}
}