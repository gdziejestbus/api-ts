const Joi = require("joi");

export const getCompanyByIdValidation = {
    params: {
        companyId: Joi.string().required()
    }
}

export const createCompanyValidation = {
    body: {
        name: Joi.string().required(),
        address: {
            street: Joi.string().required(),
            city: Joi.string().required(),
            zipCode: Joi.string().required(),
            country: Joi.string().required()
        },
        phone: Joi.string().required(),
        email: Joi.string().required()
    }
}

export const updateContactValidation = {
    body: {
        address: {
            street: Joi.string().required(),
            city: Joi.string().required(),
            zipCode: Joi.string().required(),
            country: Joi.string().required()
        },
        phone: Joi.string().required(),
        email: Joi.string().required()
    }
}

export const updateInfoValidation = {
    body: {
        name: Joi.string().required(),
        description: Joi.string().required(),
        webPage: Joi.string().required()
    }
}