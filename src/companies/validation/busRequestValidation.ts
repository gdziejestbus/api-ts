const Joi = require("joi");

export const createBusValidation = {
	body: {
		companyId: Joi.string().required(),
		vehicleModel: Joi.string().required(),
		placesAmount: Joi.number().required(),
		name: Joi.string().required()
	}
}

export const updateBusValidation = {
	body: {
		vehicleModel: Joi.string().required(),
		placesAmount: Joi.number().required(),
		name: Joi.string().required()
	}
}