import { Document, model, Schema } from 'mongoose';

export interface IBusSchema extends Document {
	companyId: string;
	vehicleModel: string;
	placesAmount: number;
	name: string;
}

const BusSchema: Schema = new Schema({
	companyId: {
		type: String,
		required: true,
		trim: true
	},
	vehicleModel: {
		type: String,
		required: true,
		trim: true
	},
	placesAmount: {
		type: Number,
		required: true,
		trim: true
	},
	name: {
		type: String,
		required: true,
		trim: true
	}
});

BusSchema.virtual('id').get(function () {
	return this._id.toHexString();
});

BusSchema.set('toObject', {
	virtuals: true
});

export default model<IBusSchema>("Bus", BusSchema);