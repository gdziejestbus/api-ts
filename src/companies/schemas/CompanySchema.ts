import { Document, model, Schema, Model } from 'mongoose';
import AddressData from '../../shared/models/AddressData';
import mongoosePaginate from 'mongoose-paginate-v2';

export interface ICompanySchema extends Document {
	name: string;
	address: AddressData;
	logoPath: string;
	webPage: string;
	phone: string;
	email: string;
	description: string;
	businessPlan: string;
	active: boolean;
	identificator: string;
}

const CompanySchema: Schema = new Schema({
	name: {
		type: String,
		required: true,
		trim: true
	},
	address: {
		street: {
			type: String,
			required: true,
			trim: true
		},
		city: {
			type: String,
			required: true,
			trim: true
		},
		zipCode: {
			type: String,
			required: true,
			trim: true
		},
		country: {
			type: String,
			required: true,
			trim: true
		},
	},
	logoPath: {
		type: String,
		trim: true
	},
	webPage: {
		type: String,
		trim: true
	},
	phone: {
		type: String,
		required: true,
		trim: true
	},
	email: {
		type: String,
		required: true,
		trim: true
	},
	description: {
		type: String,
		trim: true
	},
	businessPlan: {
		type: String,
		trim: true
	},
	identificator: {
		type: String,
		trim: true
	},
	active: {
		type: Boolean,
		default: false
	}
});

CompanySchema.virtual('id').get(function () {
	return this._id.toHexString();
});

CompanySchema.set('toObject', {
	virtuals: true
});

CompanySchema.plugin(mongoosePaginate);

interface IPaginate extends Model<ICompanySchema> {
	paginate: any;
};

export default model<ICompanySchema>("Company", CompanySchema) as IPaginate;