import { Router } from 'express';
import CompanyController from './api/controllers/CompanyController';

// validation
import { createCompanyValidation, getCompanyByIdValidation, updateContactValidation, updateInfoValidation } from './validation/companyRequestValidation';
import validate from 'express-validation';
import BusController from './api/controllers/BusController';
import { createBusValidation, updateBusValidation } from './validation/busRequestValidation';

const companyController = new CompanyController();
const busController = new BusController();

const routes: Router = Router();

// companies
routes.get('/random', companyController.getRandomCompanies);
routes.get('/search', companyController.searchCompanies);
routes.get('/:companyId', validate(getCompanyByIdValidation), companyController.getCompanyById);
routes.get('/city/:city', companyController.getCompaniesByCity);
routes.get('/identificator/:identificator', companyController.getCompanyByIdentificator);
routes.post('/create', validate(createCompanyValidation), companyController.createCompany);
routes.patch('/contact/:companyId', validate(updateContactValidation), companyController.updateContactData);
routes.patch('/info/:companyId', validate(updateInfoValidation), companyController.updateInfoData);
routes.patch('/logo/:companyId', companyController.updateLogo);
routes.delete('/:companyId', companyController.deleteById);

// buses
routes.post('/buses/create', validate(createBusValidation), busController.createBus);
routes.get('/buses/:busId', busController.getById);
routes.delete('/buses/:busId', busController.deleteById);
routes.put('/buses/:busId', validate(updateBusValidation), busController.update);
routes.get('/buses/company/:companyId', busController.getByCompanyId);

export { routes };