import Course from "../models/Course";

export interface CreateCourseRequest {
	readonly companyId: string;
	readonly stopsId: Array<string>;
	readonly courses: Array<Course>;
	readonly name: string;
	readonly type: string;
}

export interface GetByIdRequest {
	readonly courseId: string;
	readonly startStop?: string;
	readonly endStop?: string;
}

export interface GetManyRequest {
	readonly routeId: string;
}

export interface DeleteCompanyCoursesRequest {
	readonly companyId: string;
}