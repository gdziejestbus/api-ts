import TimesDetailed from "../models/TimesDetailed";
import { LatLng } from "../../shared/models/LatLng";

export default interface CourseDetailedDTO {
    readonly id: string;
    readonly routeId: string;
    readonly price?: {
        readonly normal: number;
        readonly halfPrice: number;
    };
    readonly companyId: string;
    readonly companyName: string;
    readonly times: TimesDetailed[];
    readonly companyLogo?: string;
    readonly routeName: string;
    readonly routeType: string;
    readonly coordinates?: Array<LatLng>;
}