import CourseDTO from "./CourseDTO";

export default interface RouteDetailedDTO {
	readonly id?: string;
	readonly companyId: string;
	readonly stopsId: Array<string>;
	readonly type: string;
	readonly name: string;
	readonly courses: Array<CourseDTO>;
}