import CourseDetailed from "../models/CourseDetailed";

export default interface RouteWithDetailedCoursesDTO {
	readonly id?: string;
	readonly companyId: string;
	readonly stopsId: Array<string>;
	readonly type: string;
	readonly name: string;
	readonly courses: Array<CourseDetailed>;
}