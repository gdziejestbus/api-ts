export interface GetByIdRequest {
    readonly calendarId: string
}

export interface CreateCalendarRequest {
    readonly name: string;
    readonly companyId: string;
    readonly weekDays: Array<string>;
    readonly excludeDays: Array<string>;
    readonly includeDays: Array<string>;
}

export interface UpdateCalendarRequest {
    readonly id: string,
    readonly name: string;
    readonly type: string;
    readonly companyId: string;
    readonly weekDays: Array<string>;
    readonly excludeDays: Array<string>;
    readonly includeDays: Array<string>;
}