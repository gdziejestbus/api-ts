import RouteDetailedDTO from "./RouteDetailedDTO";
import { NearestCourseDTO } from "./NearestCourseDTO";
import CourseDetailedDTO from "./CourseDetailedDTO";
import RouteWithDetailedCoursesDTO from "./RouteWithDetailedCoursesDTO";

export interface GetByIdResponse {
	readonly course: CourseDetailedDTO;
}

export interface GetManyResponse {
	readonly route: RouteDetailedDTO;
}

export interface GetManyDetailedResponse {
	readonly route: RouteWithDetailedCoursesDTO;
}

export interface GetNearestCoursesResponse {
	readonly courses: NearestCourseDTO[];
}