import CourseDetailedDTO from "./CourseDetailedDTO";

export interface SearchCoursesResponse {
	readonly courses: Array<CourseDetailedDTO>;
}