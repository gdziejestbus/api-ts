import CalendarDTO from "./CalendarDTO";

export interface GetByIdResponse {
    readonly calendar: CalendarDTO
}

export interface CreateCalendarResponse {
    readonly calendarId: string;
}

export interface GetCalendarsResponse {
    readonly calendars: CalendarDTO[];
}