import Times from "../models/Times";

export default interface CourseDTO {
    readonly id?: string;
    readonly companyId: string;
    readonly calendarId: string;
    readonly times: Times;
}