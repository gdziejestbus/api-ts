import CourseService, { ICourseService } from '../../services/CourseService';
import RouteService, { IRouteService } from '../../../routes/services/RouteService';
import { Response, Request } from 'express';
import Route from '../../../routes/models/Route';
import RouteWithCourses from '../../models/RouteWithCourses';
import { errorHandler } from '../../../shared/errors/errorHandler';
import { GetByIdResponse, GetManyResponse, GetNearestCoursesResponse, GetManyDetailedResponse } from '../../DTO/CourseResponsesDTO';
import { CreateCourseRequest, GetByIdRequest, GetManyRequest, DeleteCompanyCoursesRequest } from '../../DTO/CourseRequestsDTO';
import { toCourseDTO } from '../../converters/toCourseDTO';
import { toRouteDetailedDTO } from '../../converters/toRouteDetailedDTO';
import RouteDetailedDTO from '../../DTO/RouteDetailedDTO';
import { toNearestCourseDTO } from '../../converters/toNearestCourseDTO';
import { toCourseDetailedDTO } from '../../converters/toCourseDetailedDTO';
import { toRouteWithDetailedCoursesDTO } from '../../converters/toRouteWithDetailedCourses';
import RouteWithDetailedCoursesDTO from '../../DTO/RouteWithDetailedCoursesDTO';
import StatisticsService, { IStatisticsService } from '../../../statistics/services/StatisticsService';

export interface ICourseController {
	createCourse(req: Request, res: Response): Promise<Response>;
	getById(req: Request, res: Response): Promise<Response>;
	deleteById(req: Request, res: Response): Promise<Response>;
	getManyByRouteId(req: Request, res: Response): Promise<Response>;
	getManyDetailedByRouteId(req: Request, res: Response): Promise<Response>;
	deleteManyByRouteId(req: Request, res: Response): Promise<Response>;
	deleteCompanyCourses(req: Request, res: Response): Promise<Response>;
	getNearestStopCourses(req: Request, res: Response): Promise<Response>;
}

export default class CourseController implements ICourseController {
	private routeService: IRouteService;
	private courseService: ICourseService;
	private statisticsService: IStatisticsService;

	constructor() {
		this.routeService = new RouteService();
		this.courseService = new CourseService();
		this.statisticsService = new StatisticsService();
	}

	public getById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetByIdRequest = { ...req.params, startStop: req.query.startStop, endStop: req.query.endStop };

		try {
			const courseDetailed = await this.courseService.getById(request.courseId, request.startStop, request.endStop);

			const courseDetailedDTO = toCourseDetailedDTO(courseDetailed);

			const response: GetByIdResponse = { course: courseDetailedDTO };

			this.statisticsService.addStatistics(req.device.type, req.useragent.browser, "course_details", {
				...request,
				routeName: courseDetailed.routeName,
				companyName: courseDetailed.companyName
			});

			return res.status(200).json(response);
		} catch (err) {
			this.statisticsService.addStatistics(req.device.type, req.useragent.browser, "course_details", {
				...request,
				error: true,
				errorMessage: err.message
			});

			return errorHandler(res, err);
		}
	}

	public deleteById = async (req: Request, res: Response): Promise<Response> => {
		const request: GetByIdRequest = { ...req.params };

		try {
			await this.courseService.deleteById(request.courseId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getManyByRouteId = async (req: Request, res: Response): Promise<Response> => {
		const request: GetManyRequest = { ...req.params };

		try {
			const routeDetailed = await this.courseService.getManyByRouteId(request.routeId);

			const routeDetailedDTO: RouteDetailedDTO = {
				...toRouteDetailedDTO(routeDetailed),
				courses: routeDetailed.courses.map(course => toCourseDTO(course))
			}

			const response: GetManyResponse = { route: routeDetailedDTO };

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getManyDetailedByRouteId = async (req: Request, res: Response): Promise<Response> => {
		const request: GetManyRequest = { ...req.params };

		try {
			const routeWithDetailedCourses = await this.courseService.getManyDetailedByRouteId(request.routeId);

			const routeWithDetailedCoursesDTO: RouteWithDetailedCoursesDTO = {
				...toRouteWithDetailedCoursesDTO(routeWithDetailedCourses),
				courses: routeWithDetailedCourses.courses.map(course => toCourseDetailedDTO(course))
			}

			const response: GetManyDetailedResponse = { route: routeWithDetailedCoursesDTO };

			return res.status(200).json(response);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public deleteManyByRouteId = async (req: Request, res: Response): Promise<Response> => {
		const request: GetManyRequest = { ...req.params };

		try {
			await this.courseService.deleteManyByRouteId(request.routeId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public deleteCompanyCourses = async (req: Request, res: Response): Promise<Response> => {
		const request: DeleteCompanyCoursesRequest = { ...req.params };

		try {
			await this.courseService.deleteComapnyCourses(request.companyId);

			return res.sendStatus(204);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public createCourse = async (req: Request, res: Response): Promise<Response> => {
		const request: CreateCourseRequest = { ...req.body };

		const route: Route = { ...request }

		console.log(route);

		try {
			const routeId = await this.routeService.createRoute(route);

			const routeWithCourses: RouteWithCourses = {
				companyId: request.companyId,
				stopsId: request.stopsId,
				courses: request.courses,
				routeId
			}

			// add courses
			await this.courseService.createCourses(routeWithCourses);

			return res.sendStatus(201);
		} catch (err) {
			return errorHandler(res, err);
		}
	}

	public getNearestStopCourses = async (req: Request, res: Response): Promise<Response> => {
		try {
			const nearestCourses = await this.courseService.getNearestStopCourses(req.params.stopId);

			const nearestCoursesDTO = nearestCourses.map(nearestCourse => toNearestCourseDTO(nearestCourse));

			const response: GetNearestCoursesResponse = {
				courses: nearestCoursesDTO
			}

			this.statisticsService.addStatistics(req.device.type, req.useragent.browser, "stop_nearest_courses", {
				stopId: req.params.stopId
			});

			res.status(200).json(response);
		} catch (err) {
			this.statisticsService.addStatistics(req.device.type, req.useragent.browser, "stop_nearest_courses", {
				stopId: req.params.stopId,
				error: true,
				errorMessage: err.message
			});

			return errorHandler(res, err);
		}
	}
}