import { Response, Request } from "express";
import Calendar from '../../models/Calendar';
import CalendarService, { ICalendarService } from "../../services/CalendarService";
import { errorHandler } from "../../../shared/errors/errorHandler";
import { CreateCalendarResponse, GetByIdResponse, GetCalendarsResponse } from "../../DTO/CalendarResponsesDTO";
import { CreateCalendarRequest, UpdateCalendarRequest, GetByIdRequest } from "../../DTO/CalendarRequestsDTO";
import { toCalendarDTO } from "../../converters/toCalendarDTO";

export interface ICalendarController {
    createCalendar(req: Request, res: Response): Promise<Response>;
    getById(req: Request, res: Response): Promise<Response>;
    deleteById(req: Request, res: Response): Promise<Response>;
    update(req: Request, res: Response): Promise<Response>;
}

export default class CalendarController implements ICalendarController {
    private calendarService: ICalendarService;

    constructor() {
        this.calendarService = new CalendarService();
    }

    public getById = async (req: Request, res: Response): Promise<Response> => {
        const request: GetByIdRequest = { ...req.params };

        try {
            const calendar = await this.calendarService.getById(request.calendarId);

            const calendarDTO = toCalendarDTO(calendar);

            const response: GetByIdResponse = { calendar: calendarDTO };

            return res.status(200).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public deleteById = async (req: Request, res: Response): Promise<Response> => {
        const request: GetByIdRequest = { ...req.params };

        try {
            await this.calendarService.deleteById(request.calendarId);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public update = async (req: Request, res: Response): Promise<Response> => {
        const request: UpdateCalendarRequest = { ...req.body, id: req.params.calendarId };

        try {
            await this.calendarService.update(request);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public createCalendar = async (req: Request, res: Response): Promise<Response> => {
        const request: CreateCalendarRequest = { ...req.body };

        const calendar: Calendar = { ...request };

        try {
            const calendarId = await this.calendarService.createCalendar(calendar);

            const response: CreateCalendarResponse = { calendarId };

            return res.status(201).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
	}
	
    public getCalendars = async (req: Request, res: Response): Promise<Response> => {
        try {
            const calendars = await this.calendarService.getCalendars(req.query.companyId);

            const response: GetCalendarsResponse = {
                calendars: calendars.map(calendar => toCalendarDTO(calendar))
            }

            return res.status(200).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
    }
}