import { Response, Request } from 'express';
import SearchService, { ISearchService } from "../../services/SearchService";
import SearchCourseData from "../../models/SearchCourseData";
import CourseDetailedDTO from "../../DTO/CourseDetailedDTO";
import { toCourseDetailedDTO } from "../../converters/toCourseDetailedDTO";
import { SearchCoursesResponse } from "../../DTO/SearchResponsesDTO";
import { errorHandler } from "../../../shared/errors/errorHandler";
import StatisticsService, { IStatisticsService } from '../../../statistics/services/StatisticsService';

export interface ISearchController {
	searchCourses(req: Request, res: Response): Promise<Response>;
}

export default class SearchController implements ISearchController {
	private searchService: ISearchService;
	private statisticsService: IStatisticsService;

	constructor() {
		this.searchService = new SearchService();
		this.statisticsService = new StatisticsService();
	}

	public searchCourses = async (req: Request, res: Response): Promise<Response> => {
		const request: SearchCourseData = { ...req.query };

		try {
			const searchedCourses = await this.searchService.searchCourses(request);

			const searchedCoursesDTO: Array<CourseDetailedDTO> = searchedCourses.map(course => toCourseDetailedDTO(course));

			const response: SearchCoursesResponse = {
				courses: searchedCoursesDTO
			}

			this.statisticsService.addStatistics(req.device.type, req.useragent.browser, "search_courses", {
				...request,
				searchedCoursesAmount: searchedCourses.length
			});

			return res.status(200).json(response);
		} catch (err) {
			this.statisticsService.addStatistics(req.device.type, req.useragent.browser, "search_courses", {
				...request,
				error: true,
				errorMessage: err.message
			});

			return errorHandler(res, err);
		}
	}
}