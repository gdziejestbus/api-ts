import Course from "../models/Course";
import SearchCourseData from "../models/SearchCourseData";
import CompanyService, { ICompanyService } from "../../companies/services/CompanyService";
import CalendarService, { ICalendarService } from "./CalendarService";
import StopService, { IStopService } from "../../stops/services/StopService";
import CourseDetailed from "../models/CourseDetailed";
import Stop from "../../stops/models/Stop";
import Company from "../../companies/models/Company";
import Calendar from "../models/Calendar";
import PriceTableService, { IPriceTableService } from "../../routes/services/PriceTableService";
import StopGroupService, { IStopGroupService } from "../../stops/services/StopGroupService";
import Route from "../../routes/models/Route";
import RouteService, { IRouteService } from "../../routes/services/RouteService";
import CourseService, { ICourseService } from "./CourseService";
import PriceTable from "../../routes/models/PriceTable";
import TimesDetailed from "../models/TimesDetailed";

export interface ISearchService {
	searchCourses(routeWithCourses: SearchCourseData): Promise<Array<CourseDetailed>>;
}

export default class SearchService implements ISearchService {
	private calendarService: ICalendarService;
	private companyService: ICompanyService;
	private stopService: IStopService;
	private priceTableService: IPriceTableService;
	private stopGroupService: IStopGroupService;
	private courseService: ICourseService;
	private routeService: IRouteService;

	constructor() {
		this.calendarService = new CalendarService();
		this.companyService = new CompanyService();
		this.stopGroupService = new StopGroupService();
		this.stopService = new StopService();
		this.priceTableService = new PriceTableService();
		this.courseService = new CourseService();
		this.routeService = new RouteService();
	}

	public searchCourses = async (searchData: SearchCourseData): Promise<Array<CourseDetailed>> => {
		const fromStopGroup = await this.stopGroupService.getById(searchData.from);
		const toStopGroup = await this.stopGroupService.getById(searchData.to);

		const fromStopsArray: Array<string> = fromStopGroup.stopsId;
		const toStopsArray: Array<string> = toStopGroup.stopsId;

		const routes = await this.getRoutes(fromStopsArray, toStopsArray);

		const courses = await this.getCourses(routes);

		const startStop = this.getStop(courses, fromStopsArray);
		const endStop = this.getStop(courses, toStopsArray);

		return this.filterCourses(courses, routes, startStop, endStop, searchData);
	}

	private filterCourses = async (courses: Course[], routes: Route[], startStop: string, endStop: string, searchData: SearchCourseData) => {
		const calendarsId = this.getCalendarsId(courses);

		const calendars = await this.calendarService.getMany(calendarsId);

		const filteredCoursesByDay = this.filterCoursesByDay(courses, calendars, searchData);

		const filteredCoursesByTime = this.filterCoursesByTime(filteredCoursesByDay, searchData, startStop, endStop);

		const sortedCourses = this.sortCoursesByTime(filteredCoursesByTime, searchData, startStop, endStop);

		const coursesWithNames = await this.assignNamesToCourses(sortedCourses, routes);

		const assignedCourses = await this.addPricesToCourses(coursesWithNames, startStop, endStop);

		return this.cropUnusedStops(assignedCourses, startStop, endStop);
	}

	private cropUnusedStops(courses: CourseDetailed[], startStop: string, endStop: string): CourseDetailed[] {
		return courses.map(course => {
			const startIndex = course.times.findIndex(x => x.id === startStop);
			const endIndex = course.times.findIndex(x => x.id === endStop);

			return {
				...course,
				times: course.times.slice(startIndex, endIndex + 1)
			}
		});
	}

	private addPricesToCourses = async (courses: Array<CourseDetailed>, startStop: string, endStop: string): Promise<Array<CourseDetailed>> => {
		const routesId = this.getRoutesIdFromCourses(courses);

		const priceTables = await this.priceTableService.getPriceTableByManyRoutesId(routesId);

		return this.getCoursesWithPrices(courses, priceTables, startStop, endStop);
	}

	private getRoutesIdFromCourses(courses: Array<CourseDetailed>): Array<string> {
		const routesId: Array<string> = [];

		courses.forEach(course => {
			if (!routesId.includes(course.routeId)) {
				routesId.push(course.routeId);
			}
		});

		return routesId;
	}

	private getCoursesWithPrices(courses: Array<CourseDetailed>, priceTables: Array<PriceTable>, startStop: string, endStop: string) {
		return courses.map(course => {
			const priceTable = priceTables.find(x => x.routeId === course.routeId);

			const courseWithPrice = {
				...course,
				price: priceTable ? {
					normal: priceTable.normal ? priceTable.normal[startStop][endStop] : null,
					halfPrice: priceTable.halfPrice ? priceTable.halfPrice[startStop][endStop] : null
				} : null
			}

			return courseWithPrice;
		});
	}

	private assignNamesToCourses = async (courses: Array<Course>, routes: Route[]): Promise<Array<CourseDetailed>> => {
		const stopsId: Array<string> = [];
		const companiesId: Array<string> = [];
		const coursesToReturn: Array<CourseDetailed> = [];

		courses.forEach(course => {
			if (!companiesId.includes(course.companyId)) {
				companiesId.push(course.companyId);
			}

			for (const stopId in course.times) {
				if (!stopsId.includes(stopId)) {
					stopsId.push(stopId);
				}
			}
		});

		const stops: Array<Stop> = await this.stopService.getMany(stopsId);
		const companies: Array<Company> = await this.companyService.getMany(companiesId);

		courses.forEach(course => {
			const times = this.addNameToStop(course, stops);

			coursesToReturn.push(this.toDetailedCourse(course, times, companies, routes));
		});

		return coursesToReturn;
	}

	private addNameToStop(course: Course, stops: Array<Stop>): Array<TimesDetailed> {
		const times: Array<TimesDetailed> = [];

		for (const stopId in course.times) {
			const stop = stops.find(x => x.id === stopId);

			times.push({
				id: stop.id,
				name: stop.name,
				arrival: course.times[stopId].arrival,
				departure: course.times[stopId].departure
			})
		}

		return times;
	}

	private toDetailedCourse(course: Course, times: Array<TimesDetailed>, companies: Company[], routes: Route[]): CourseDetailed {
		const company = companies.find(company => course.companyId === company.id) as Company;
		const route = routes.find(route => route.id === course.routeId);

		return {
			id: course.id,
			routeId: course.routeId,
			companyId: course.companyId,
			times,
			companyName: company.name,
			companyLogo: company.logoPath,
			routeName: route.name,
			routeType: route.type
		} as CourseDetailed;
	}

	private getStop(courses: Array<Course>, stops: Array<string>) {
		return stops.filter(stop => courses[0].times.hasOwnProperty(stop))[0];
	}

	private getRoutes = async (fromStopsArray: Array<string>, toStopsArray: Array<string>): Promise<Array<Route>> => {
		return await this.routeService.findRoutes(fromStopsArray, toStopsArray);
	}

	private getCourses = async (routes: Array<Route>): Promise<Array<Course>> => {
		const routesIdArray = this.getRoutesId(routes);

		return await this.courseService.getManyByRoutesId(routesIdArray);
	}

	private filterCoursesByTime = (courses: Array<Course>, searchData: SearchCourseData, startStop: string, endStop: string): Array<Course> => {
		const filteredCourses: Array<Course> = [];

		if (searchData.arrival) {
			courses.forEach(course => {
				if (this.compareTime(course.times[endStop].arrival, searchData.arrival)) {
					filteredCourses.push(course);
				}
			});
		} else if (searchData.departure) {
			courses.forEach(course => {
				if (this.compareTime(searchData.departure, course.times[startStop].departure)) {
					filteredCourses.push(course);
				}
			});
		}

		return filteredCourses;
	}

	private sortCoursesByTime = (courses: Array<Course>, searchData: SearchCourseData, startStop: string, endStop: string): Array<Course> => {
		if (searchData.arrival) {
			return courses.sort((a, b) => {
				if (this.compareTime(a.times[startStop].arrival, b.times[startStop].arrival)) return 1;
				else return -1;
			});
		} else if (searchData.departure) {
			return courses.sort((a, b) => {
				if (this.compareTime(a.times[startStop].departure, b.times[startStop].departure)) return -1;
				else return 1;
			});
		}
	}

	private compareTime = (str1, str2) => {
		if (str1 === str2) {
			return true;
		}

		var time1 = str1.split(':');
		var time2 = str2.split(':');

		if (parseInt(time1[0]) > parseInt(time2[0])) {
			return false;
		} else if (parseInt(time1[0]) == parseInt(time2[0]) && parseInt(time1[1]) > parseInt(time2[1])) {
			return false;
		} else {
			return true;
		}
	}

	private filterCoursesByDay = (courses: Array<Course>, calendars: Array<Calendar>, searchData: SearchCourseData): Array<Course> => {
		const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		const filteredCourses: Array<Course> = [];
		const dayOfWeek = DAYS[new Date(searchData.day).getDay()];

		courses.forEach(course => {
			const courseCalendar = calendars.find(calendar => calendar.id === course.calendarId);

			if (this.isValidForDate(courseCalendar, searchData, dayOfWeek)) {
				filteredCourses.push(course);
			}
		});

		return filteredCourses;
	}

	private isValidForDate(courseCalendar: Calendar, searchData: SearchCourseData, dayOfWeek: string) {
		return (!courseCalendar.excludeDays.includes(searchData.day) && courseCalendar.weekDays.includes(dayOfWeek)) || courseCalendar.includeDays.includes(searchData.day);
	}

	private getCalendarsId = (courses: Array<Course>): Array<string> => {
		const calendarsId = [];

		courses.forEach(course => {
			if (calendarsId.indexOf(course.calendarId) === -1) {
				calendarsId.push(course.calendarId);
			}
		});

		return calendarsId;
	}

	private getRoutesId = (routes: Array<Route>): Array<string> => {
		const routesId = [];

		routes.forEach(route => {
			routesId.push(route.id);
		});

		return routesId;
	}
}