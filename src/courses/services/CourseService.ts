import RouteWithCourses from "../models/RouteWithCourses";
import CourseRepository, { ICourseRepository } from '../repositories/CourseRepository';
import Course from "../models/Course";
import RouteService, { IRouteService } from "../../routes/services/RouteService";
import RouteDetailed from "../models/RouteDetailed";
import momentTimezone from 'moment-timezone';
import CompanyService, { ICompanyService } from "../../companies/services/CompanyService";
import { NearestCourse } from "../models/NearestCourse";
import StopService, { IStopService } from "../../stops/services/StopService";
import StopRoute from "../../routes/models/StopRoutes";
import CourseDetailed from "../models/CourseDetailed";
import Stop from "../../stops/models/Stop";
import Company from "../../companies/models/Company";
import Route from "../../routes/models/Route";
import TimesDetailed from "../models/TimesDetailed";
import RouteWithDetailedCourses from "../models/RouteWithDetailedCourses";

export interface ICourseService {
	createCourses(routeWithCourses: RouteWithCourses): Promise<Array<string>>;
	getById(id: string, startStop?: string, endStop?: string): Promise<CourseDetailed>;
	deleteById(id: string): Promise<void>;
	getManyByRouteId(routeId: string): Promise<RouteDetailed>;
	getManyDetailedByRouteId(routeId: string): Promise<RouteWithDetailedCourses>;
	getManyByRoutesId(routesId: Array<string>): Promise<Array<Course>>;
	deleteManyByRouteId(routeId: string): Promise<void>;
	deleteComapnyCourses(companyId: string): Promise<void>;
	getNearestStopCourses(stopId: string): Promise<Array<NearestCourse>>;
}

export default class CourseService implements ICourseService {
	private courseRepository: ICourseRepository;
	private routeService: IRouteService;
	private companyService: ICompanyService;
	private stopService: IStopService;

	constructor() {
		this.courseRepository = new CourseRepository();
		this.routeService = new RouteService();
		this.companyService = new CompanyService();
		this.stopService = new StopService();
	}

	public async getById(id: string, startStop?: string, endStop?: string): Promise<CourseDetailed> {
		const course = await this.courseRepository.getById(id);

		if (!course) throw new Error("NotFound");

		const route = await this.routeService.getRouteById(course.routeId);
		const company = await this.companyService.getCompanyById(course.companyId);
		const stops = await this.getStops(course.times);

		let times = this.addNameToStop(course, stops);

		const croppedRoute = this.cropRouteCoordinates(route, startStop, endStop);

		return this.toDetailedCourse(course, times, company, croppedRoute);
	}

	private cropRouteCoordinates(route: Route, startStop: string, endStop: string): Route {
		if (!startStop || !endStop) {
			return route;
		}

		const startIndex = route.stopsId.findIndex(id => id === startStop);
		const endIndex = route.stopsId.findIndex(id => id === endStop);

		if (startIndex === -1 || endIndex === -1) {
			return route;
		}

		return {
			...route,
			coordinates: route.coordinates.slice(startIndex, endIndex)
		}
	}

	public async deleteById(id: string): Promise<void> {
		await this.courseRepository.deleteById(id);
	}

	public createCourses = async (routeWithCourses: RouteWithCourses): Promise<Array<string>> => {
		const coursesArray: Array<Course> = [];

		routeWithCourses.courses.forEach((course: Course) => {
			const createdCourse: Course = {
				...course,
				companyId: routeWithCourses.companyId,
				routeId: routeWithCourses.routeId
			};

			coursesArray.push(createdCourse);
		});

		return await this.courseRepository.createMany(coursesArray);
	}

	public getManyByRouteId = async (routeId: string): Promise<RouteDetailed> => {
		const courses = await this.courseRepository.getManyByRouteId(routeId);
		const route = await this.routeService.getRouteById(routeId);

		const routeDetailed: RouteDetailed = {
			...route,
			courses
		}

		if (courses.length === 0) throw new Error("NotFound");

		return routeDetailed;
	}


	public getManyDetailedByRouteId = async (routeId: string): Promise<RouteWithDetailedCourses> => {
		const courses = await this.courseRepository.getManyByRouteId(routeId);
		const route = await this.routeService.getRouteById(routeId);
		const stops = await this.stopService.getMany(route.stopsId);
		const company = await this.companyService.getCompanyById(route.companyId);

		if (courses.length === 0) throw new Error("NotFound");

		const routeWithDetailedCourses: RouteWithDetailedCourses = {
			...route,
			courses: courses.map(course => {
				return {
					...this.toDetailedCourse(course, course.times, company, route),
					times: this.addNameToStop(course, stops),
					coordinates: null
				}
			})
		}

		return routeWithDetailedCourses;
	}

	public getManyByRoutesId = async (routesId: Array<string>): Promise<Array<Course>> => {
		return await this.courseRepository.findMany(routesId);
	}

	public deleteManyByRouteId = async (routeId: string): Promise<void> => {
		await this.courseRepository.deleteManyByRouteId(routeId);

		await this.routeService.deleteById(routeId);
	}

	public deleteComapnyCourses = async (companyId: string): Promise<void> => {
		await this.courseRepository.deleteManyByCompanyId(companyId);

		await this.routeService.deleteCompanyRoutes(companyId);
	}

	public getNearestStopCourses = async (stopId: string): Promise<Array<NearestCourse>> => {
		const routes = await this.routeService.getStopRoutes(stopId);

		const { companiesId, routesId, endStopsId } = this.getRequiredModelsId(routes);

		const { companies, stops, courses } = await this.getModels(companiesId, routesId, endStopsId);

		const nearestCourses = this.getNearestCourses(courses, companies, routes, stops, stopId);

		const now = momentTimezone(Date.now() - 1000 * 60 * 5);
		const time = now.tz("Europe/Warsaw").format("HH:mm");

		const filteredNearestCourses = nearestCourses.filter(x => x.time.departure >= time);

		return filteredNearestCourses.sort(this.sortCourses);
	}

	private sortCourses(a, b) {
		if (a.time.departure < b.time.departure) return -1;
		if (a.time.departure > b.time.departure) return 1;
		return 0;
	}

	private getNearestCourses(courses, companies, routes, stops, stopId): Array<NearestCourse> {
		return courses.map(course => {
			const company = companies.find(x => x.id === course.companyId);
			const route = routes.find(x => x.routeId === course.routeId);
			const endStop = stops.find(x => x.id === route.stopsId[route.stopsId.length - 1]);

			return {
				type: route.routeType,
				companyName: company.name,
				companyId: company.id,
				endStop: endStop.name,
				active: false, // for now false, we will handle it when we will have bus localizations
				time: course.times[stopId]
			};
		});
	}

	private async getModels(companiesId, routesId, endStopsId) {
		const companies = await this.companyService.getMany(companiesId);

		const courses = await this.courseRepository.findMany(routesId);

		const stops = await this.stopService.getMany(endStopsId);

		return { companies, courses, stops };
	}

	private getRequiredModelsId(routes: StopRoute[]) {
		const companiesId: string[] = [];
		const routesId: string[] = [];
		const endStopsId: string[] = [];

		routes.forEach(route => {
			const endStopId = route.stopsId[route.stopsId.length - 1];

			if (!endStopsId.includes(endStopId)) {
				endStopsId.push(endStopId);
			}

			if (!companiesId.includes(route.companyId)) {
				companiesId.push(route.companyId);
			}

			if (!routesId.includes(route.routeId)) {
				routesId.push(route.routeId);
			}
		});

		return { companiesId, routesId, endStopsId };
	}

	private addNameToStop(course: Course, stops: Array<Stop>): Array<TimesDetailed> {
		const times: Array<TimesDetailed> = [];

		for (const stopId in course.times) {
			const stop = stops.find(x => x.id === stopId);

			times.push({
				id: stop.id,
				name: stop.name,
				arrival: course.times[stopId].arrival,
				departure: course.times[stopId].departure
			})
		}

		return times;
	}

	private toDetailedCourse(course: Course, times: Object, company: Company, route: Route): CourseDetailed {
		return {
			id: course.id,
			routeId: course.routeId,
			companyId: course.companyId,
			times,
			companyName: company.name,
			companyLogo: company.logoPath,
			routeName: route.name,
			routeType: route.type,
			coordinates: [].concat(...route.coordinates)
		} as CourseDetailed;
	}

	private async getStops(times): Promise<Array<Stop>> {
		const stopsId: Array<string> = [];

		for (const stopId in times) {
			if (!stopsId.includes(stopId)) {
				stopsId.push(stopId);
			}
		}

		return await this.stopService.getMany(stopsId);
	}
}