import Calendar from "../models/Calendar";
import CalendarRepository, { ICalendarRepository } from "../repositories/CalendarRepository";

export interface ICalendarService {
	createCalendar(stop: Calendar): Promise<string>;
	getById(id: string): Promise<Calendar>;
	deleteById(id: string): Promise<void>;
	update(calendar: Calendar): Promise<void>;
	getMany(calendarsId: Array<string>): Promise<Array<Calendar>>;
	getCalendars(companyId: string): Promise<Array<Calendar>>;
}

export default class CalendarService implements ICalendarService {
	private calendarRepository: ICalendarRepository;

	constructor() {
		this.calendarRepository = new CalendarRepository();
	}

	public async getById(id: string): Promise<Calendar> {
		const calendar = await this.calendarRepository.getById(id);

		if (!calendar) throw new Error("NotFound");

		return calendar;
	}

	public async deleteById(id: string): Promise<void> {
		await this.calendarRepository.deleteById(id);
	}

	public async update(calendar: Calendar): Promise<void> {
		await this.calendarRepository.update(calendar);
	}

	public async createCalendar(calendar: Calendar): Promise<string> {
		const createdCalendar = await this.calendarRepository.create(calendar);

		return createdCalendar.id;
	}

	public async getMany(calendarsId: Array<string>): Promise<Array<Calendar>> {
		const calendars = await this.calendarRepository.findMany(calendarsId);

		if (calendars.length === 0) throw new Error("NotFound");

		return calendars;
	}

	public async getCalendars(companyId: string): Promise<Array<Calendar>> {
		return await this.calendarRepository.getCalendars(companyId);
	}
}