import { Document, model, Schema } from 'mongoose';
import Times from '../models/Times';

export interface ICourseSchema extends Document {
    companyId: string;
    routeId: string;
    calendarId: string;
    times: Times
}

const CourseSchema: Schema = new Schema({
    companyId: {
        type: String,
        required: true,
        trim: true
    },
    routeId: {
        type: String,
        required: true,
        trim: true
    },
    calendarId: {
        type: String,
        required: true,
        trim: true
    },
    times: {
        type: Object
    }
});

CourseSchema.virtual('id').get(function () {
	return this._id.toHexString();
});

CourseSchema.set('toObject', {
	virtuals: true
});

export default model<ICourseSchema>("Course", CourseSchema);