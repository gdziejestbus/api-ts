import { Document, model, Schema } from 'mongoose';

export interface ICalendarSchema extends Document {
    readonly id: string;
    readonly companyId: string;
    readonly name: string;
    readonly type: string;
    readonly default: number;
    readonly weekDays: Array<string>;
    readonly includeDays: Array<string>;
    readonly excludeDays: Array<string>;
}

const CalendarSchema: Schema = new Schema({
    companyId: {
        type: String,
        trim: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    type: {
        type: String,
        default: "company"
    },
    default: {
        type: Number,
        default: 0
    },
    weekDays: {
        type: [String]
    },
    includeDays: {
        type: [String]
    },
    excludeDays: {
        type: [String]
    }
});

CalendarSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

CalendarSchema.set('toObject', {
    virtuals: true
});

export default model<ICalendarSchema>("Calendar", CalendarSchema);