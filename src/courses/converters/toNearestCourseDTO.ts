import { NearestCourse } from "../models/NearestCourse";
import { NearestCourseDTO } from "../DTO/NearestCourseDTO";

export function toNearestCourseDTO(nearestCourse: NearestCourse): NearestCourseDTO {
    const courseDTO: NearestCourseDTO = {
        type: nearestCourse.type,
        companyName: nearestCourse.companyName,
        companyId: nearestCourse.companyId,
        endStop: nearestCourse.endStop,
        time: nearestCourse.time,
        active: nearestCourse.active
    }

    return courseDTO;
}