import RouteDetailed from "../models/RouteDetailed";
import RouteDetailedDTO from "../DTO/RouteDetailedDTO";

export function toRouteDetailedDTO(routeDetailed: RouteDetailed): RouteDetailedDTO {
	const routeDetailedDTO: RouteDetailedDTO = {
		id: routeDetailed.id,
		stopsId: routeDetailed.stopsId,
		companyId: routeDetailed.companyId,
		name: routeDetailed.name,
		type: routeDetailed.type,
		courses: routeDetailed.courses
	}
	return routeDetailedDTO;
}