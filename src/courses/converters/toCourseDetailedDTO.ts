import CourseDetailed from "../models/CourseDetailed";
import CourseDetailedDTO from "../DTO/CourseDetailedDTO";

export function toCourseDetailedDTO(courseDetailed: CourseDetailed): CourseDetailedDTO {
    const courseDetailedDTO: CourseDetailedDTO = {
        id: courseDetailed.id,
        routeId: courseDetailed.routeId,
        routeType: courseDetailed.routeType,
        routeName: courseDetailed.routeName,
        companyId: courseDetailed.companyId,
        companyName: courseDetailed.companyName,
        companyLogo: courseDetailed.companyLogo,
        price: courseDetailed.price ? courseDetailed.price : null,
        coordinates: courseDetailed.coordinates ? courseDetailed.coordinates : null,
        times: courseDetailed.times
    }

    return courseDetailedDTO;
}