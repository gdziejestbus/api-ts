import Calendar from "../models/Calendar";
import CalendarDTO from "../DTO/CalendarDTO";

export function toCalendarDTO(calendar: Calendar): CalendarDTO {
    const calendarDTO: CalendarDTO = {
        id: calendar.id,
        name: calendar.name,
        type: calendar.type,
        default: calendar.default,
        companyId: calendar.companyId,
        weekDays: calendar.weekDays,
        excludeDays: calendar.excludeDays,
        includeDays: calendar.includeDays
    }

    return calendarDTO;
}