import RouteWithDetailedCourses from "../models/RouteWithDetailedCourses";
import RouteWithDetailedCoursesDTO from "../DTO/RouteWithDetailedCoursesDTO";

export function toRouteWithDetailedCoursesDTO(routeWithDetailedCourses: RouteWithDetailedCourses): RouteWithDetailedCoursesDTO {
	const routeWithDetailedCoursesDTO: RouteWithDetailedCoursesDTO = {
		id: routeWithDetailedCourses.id,
		stopsId: routeWithDetailedCourses.stopsId,
		companyId: routeWithDetailedCourses.companyId,
		name: routeWithDetailedCourses.name,
		type: routeWithDetailedCourses.type,
		courses: routeWithDetailedCourses.courses
	}
	return routeWithDetailedCoursesDTO;
}