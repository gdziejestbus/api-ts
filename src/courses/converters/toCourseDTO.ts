import Course from "../models/Course";
import CourseDTO from "../DTO/CourseDTO";

export function toCourseDTO(course: Course): CourseDTO {
    const courseDTO: CourseDTO = {
        id: course.id,
        companyId: course.companyId,
        times: course.times,
        calendarId: course.calendarId,
    }

    return courseDTO;
}