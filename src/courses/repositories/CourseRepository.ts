import Course from "../models/Course";
import CourseSchema from "../schemas/CourseSchema";
import Repository from "../../shared/repository/Repository";
import IRepository from "../../shared/repository/IRepository";

export interface ICourseRepository extends IRepository<Course> {
	createMany(coursesArray: Array<Course>): Promise<Array<string>>;
	findMany(routesIdArray: Array<string>): Promise<Array<Course>>;
	getManyByRouteId(routeId: string): Promise<Array<Course>>;
	deleteManyByRouteId(coursesIdArray: string): Promise<void>;
	deleteManyByCompanyId(companyId: string): Promise<void>;
}

export default class CourseRepository extends Repository<Course> implements ICourseRepository {
	constructor() {
		super(CourseSchema);
	}

	public createMany = async (coursesArray: Array<Course>): Promise<Array<string>> => {
		const createdCourses = await CourseSchema.insertMany(coursesArray);

		const createdCoursesIdArray: Array<string> = [];

		createdCourses.forEach(course => {
			createdCoursesIdArray.push(course._id);
		});

		return createdCoursesIdArray;
	}

	public findMany = async (routesIdArray: Array<string>): Promise<Array<Course>> => {
		const courses = await CourseSchema.find({ routeId: { $in: routesIdArray } });

		return courses.length > 0 ? courses.map(course => course.toObject()) : [];
	}


	public getManyByRouteId = async (routeId: string): Promise<Course[]> => {
		const courses = await CourseSchema.find({ routeId: routeId });

		return courses.length > 0 ? courses.map(course => course.toObject()) : [];
	}

	public deleteManyByRouteId = async (routeId: string): Promise<void> => {
		await CourseSchema.deleteMany({ routeId: routeId });
	}

	public deleteManyByCompanyId = async (companyId: string): Promise<void> => {
		await CourseSchema.deleteMany({ companyId });
	}
}