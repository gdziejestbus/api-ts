import Calendar from "../models/Calendar";
import IRepository from "../../shared/repository/IRepository";
import CalendarSchema from '../schemas/CalendarSchema';
import Repository from "../../shared/repository/Repository";

export interface ICalendarRepository extends IRepository<Calendar> {
    findMany(calendarsId: Array<string>): Promise<Array<Calendar>>;
    getCalendars(companyId: string): Promise<Array<Calendar>>;
}

export default class StopRepository extends Repository<Calendar> implements ICalendarRepository {
    constructor() {
        super(CalendarSchema);
    }

    public findMany = async (calendarsId: Array<string>): Promise<Array<Calendar>> => {
        const calendars = await CalendarSchema.find({ _id: { $in: calendarsId } });

        return calendars.length > 0 ? calendars.map(calendar => calendar.toObject()) : [];
    }

    public getCalendars = async (companyId: string): Promise<Array<Calendar>> => {
        // const query = companyId ? ;

        const calendars = await CalendarSchema.find({ $or: [{ type: "predefined" }, { companyId: companyId }] });

        return calendars.length > 0 ? calendars.map(calendar => calendar.toObject()) : [];
    }
}