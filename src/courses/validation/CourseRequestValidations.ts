const Joi = require("joi");

export const createCourseValidation = {
    body: {
        companyId: Joi.string().required(),
        stopsId: Joi.array().items(Joi.string()).required(),
        courses: Joi.array().items(Joi.object())
    }
}

export const createCalendarValidation = {
    body: {
        companyId: Joi.string(),
        name: Joi.string().required(),
        weekDays: Joi.array().items(Joi.string()).required(),
        excludeDays: Joi.array().items(Joi.string()).required(),
        includeDays: Joi.array().items(Joi.string()).required()
    }
}