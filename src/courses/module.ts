import { Router } from 'express';
import CourseController from './api/controller/CourseController';
import CalendarController from './api/controller/CalendarController';
import SearchController from './api/controller/SearchController';

// validation
import { createCourseValidation, createCalendarValidation } from './validation/CourseRequestValidations';
import validate from 'express-validation';

const courseController = new CourseController();
const calendarController = new CalendarController();
const searchController = new SearchController();

const routes: Router = Router();

// courses
routes.post('/courses/create', validate(createCourseValidation), courseController.createCourse);
routes.get('/courses/search', searchController.searchCourses);
routes.get('/courses/:courseId', courseController.getById);
routes.delete('/courses/:courseId', courseController.deleteById);
routes.delete('/courses/route/:routeId', courseController.deleteManyByRouteId);
routes.get('/courses/stop/:stopId', courseController.getNearestStopCourses);
routes.delete('/courses/company/:companyId', courseController.deleteCompanyCourses);
routes.get('/courses/route/:routeId', courseController.getManyByRouteId);
routes.get('/courses/route/:routeId/detailed', courseController.getManyDetailedByRouteId);

// calendars
routes.get('/calendars/company', calendarController.getCalendars);
routes.get('/calendars/:calendarId', calendarController.getById);
routes.put('/calendars/:calendarId', validate(createCalendarValidation), calendarController.update);
routes.delete('/calendars/:calendarId', calendarController.deleteById);
routes.post('/calendars/create', validate(createCalendarValidation), calendarController.createCalendar);

export { routes };