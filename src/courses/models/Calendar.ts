export default interface Calendar {
    readonly id?: string;
    readonly companyId?: string;
    readonly name: string;
    readonly type?: string;
    readonly default?: number;
    readonly weekDays: Array<string>;
    readonly includeDays: Array<string>;
    readonly excludeDays: Array<string>;
}