export interface NearestCourse {
    readonly type: string;
    readonly companyName: string;
    readonly companyId: string;
    readonly endStop: string;
    readonly time: {
        readonly departure: string;
        readonly arrival: string;
    };
    readonly active: boolean;
}