import CourseDetailed from "./CourseDetailed";

export default interface RouteWithDetailedCourses {
	readonly id?: string;
	readonly companyId: string;
	readonly stopsId: Array<string>;
	readonly type: string;
	readonly name: string;
	readonly courses: Array<CourseDetailed>;
}