export default interface SearchCourseData {
	readonly from: string;
	readonly to: string;
	readonly day: string;
	readonly arrival?: string;
	readonly departure?: string;
}