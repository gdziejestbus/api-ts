export default interface TimesDetailed {
    readonly id: string;
    readonly name: string;
    readonly arrival: string;
    readonly departure: string;
}