import Course from "./Course";

export default interface RouteDetailed {
	readonly id?: string;
	readonly companyId: string;
	readonly stopsId: Array<string>;
	readonly type: string;
	readonly name: string;
	readonly courses: Array<Course>;
}