import Course from "./Course";

export default interface RouteWithCourses {
	readonly companyId: string;
	readonly routeId: string;
	readonly stopsId: Array<string>;
	readonly courses: Array<Course>
}