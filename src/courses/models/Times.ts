export default interface Times {
    readonly [stopId: string]: {
        readonly arrival: string,
        readonly departure: string
    }
}