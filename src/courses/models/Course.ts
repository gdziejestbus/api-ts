import Times from "./Times";

export default interface Course {
    readonly id?: string;
    readonly routeId?: string;
    readonly companyId: string;
    readonly calendarId: string;
    readonly times: Times;
}