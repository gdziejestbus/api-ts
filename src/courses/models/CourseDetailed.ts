import TimesDetailed from "./TimesDetailed";
import { LatLng } from "../../shared/models/LatLng";

export default interface CourseDetailed {
    readonly id?: string;
    readonly routeId?: string;
    readonly price?: {
        readonly normal: number;
        readonly halfPrice: number;
    };
    readonly companyId: string;
    readonly companyName: string;
    readonly times: TimesDetailed[];
    readonly routeName: string;
    readonly companyLogo?: string;
    readonly routeType: string;
    readonly coordinates?: Array<LatLng>;
}