import { Document, model, Schema } from 'mongoose';

export interface IOwnerSchema extends Document {
    readonly companies: string[];
    readonly firstName: string;
    readonly surname: string;
}

const OwnerSchema: Schema = new Schema({
    companies: {
        type: [String],
        required: true
    },
    firstName: {
        type: String,
        trim: true
    },
    surname: {
        type: [String],
        trim: true
    }
});

OwnerSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

OwnerSchema.set('toObject', {
    virtuals: true
});

export default model<IOwnerSchema>("Owner", OwnerSchema);