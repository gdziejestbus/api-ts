import { Document, model, Schema } from 'mongoose';

export interface INormalUserSchema extends Document {
    readonly status: string;
    readonly favourites: {
        readonly routes: string[];
        readonly stops: string[];
        readonly companies: string[];
    };
}

const NormalUserSchema: Schema = new Schema({
    status: {
        type: String,
        trim: true
    },
    favourites: {
        routes: {
            type: [String]
        },
        stops: {
            type: [String]
        },
        companies: {
            type: [String]
        }
    }
});

NormalUserSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

NormalUserSchema.set('toObject', {
    virtuals: true
});

export default model<INormalUserSchema>("NormalUser", NormalUserSchema);