import { Document, model, Schema } from 'mongoose';

export interface IAmbassadorSchema extends Document {
    readonly city: string;
    readonly zipCode: string;
    readonly street: string;
    readonly firstName: string;
    readonly surname: string;
    readonly promoCode: string;
    readonly stats: {
        readonly promoCodeUses: number;
        readonly addedRoutes: number;
        readonly confirmedRoutes: number;
    };
}

const AmbassadorSchema: Schema = new Schema({
    city: {
        type: String,
        required: true
    },
    zipCode: {
        type: String,
        trim: true
    },
    firstName: {
        type: String,
        trim: true
    },
    surname: {
        type: String,
        trim: true
    },
    promoCode: {
        type: String,
        trim: true
    },
    stats: {
        promoCodeUses: {
            type: Number,
            default: 0
        },
        addedRoutes: {
            type: Number,
            default: 0
        },
        confirmedRoutes: {
            type: Number,
            default: 0
        }
    }
});

AmbassadorSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

AmbassadorSchema.set('toObject', {
    virtuals: true
});

export default model<IAmbassadorSchema>("Ambassador", AmbassadorSchema);