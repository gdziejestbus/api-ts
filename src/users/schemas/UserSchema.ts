import { Document, model, Schema } from 'mongoose';

export interface IUserSchema extends Document {
    readonly login: string;
    readonly password: string;
    readonly email: string;
    readonly role: string[];
    readonly city: string;
    readonly birthDate: Date;
}

const UserSchema: Schema = new Schema({
    login: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    role: {
        type: String,
        default: "normal",
        trim: true
    },
    city: {
        type: String,
        trim: true
    },
    birthDate: {
        type: Date
    }
});

UserSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

UserSchema.set('toObject', {
    virtuals: true
});

export default model<IUserSchema>("User", UserSchema);