const Joi = require("joi");
import config from '../../shared/config';

export const createUserValidation = {
    body: {
        login: Joi.string().required(),
        password: Joi.string().regex(config.PASSWORD_REGEX).required(),
        email: Joi.string().regex(config.EMAIL_REGEX).required()
    }
}

export const updateUserValidation = {
    body: {
        login: Joi.string().required(),
        city: Joi.string(),
        birthDate: Joi.date(),
        email: Joi.string().regex(config.EMAIL_REGEX).required()
    }
}

export const updatePasswordValidation = {
    body: {
        oldPassword: Joi.string().regex(config.PASSWORD_REGEX).required(),
        newPassword: Joi.string().regex(config.PASSWORD_REGEX).required()
    }
}

export const updateAmbassadorValidation = {
    body: {
        city: Joi.string().required(),
        zipCode: Joi.string().required(),
        street: Joi.string().required(),
        firstName: Joi.string().required(),
        surname: Joi.string().required()
    }
}