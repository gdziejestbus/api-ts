import Ambassador from "../models/Ambassador";
import AmbassadorSchema from "../schemas/AmbassadorSchema";
import IRepository from "../../shared/repository/IRepository";
import Repository from "../../shared/repository/Repository";

export interface IAmbassadorRepository extends IRepository<Ambassador> {

}

export default class AmbassadorRepository extends Repository<Ambassador> implements IAmbassadorRepository {
    constructor() {
        super(AmbassadorSchema);
    }
}