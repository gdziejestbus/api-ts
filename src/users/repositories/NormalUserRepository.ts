import NormalUser from "../models/NormalUser";
import NormalUserSchema from "../schemas/NormalUserSchema";
import IRepository from "../../shared/repository/IRepository";
import Repository from "../../shared/repository/Repository";

export interface INormalUserRepository extends IRepository<NormalUser> {

}

export default class NormalUserRepository extends Repository<NormalUser> implements INormalUserRepository {
    constructor() {
        super(NormalUserSchema);
    }
}