import Owner from "../models/Owner";
import OwnerSchema from "../schemas/OwnerSchema";
import IRepository from "../../shared/repository/IRepository";
import Repository from "../../shared/repository/Repository";

export interface IOwnerRepository extends IRepository<Owner> {

}

export default class OwnerRepository extends Repository<Owner> implements IOwnerRepository {
    constructor() {
        super(OwnerSchema);
    }
}