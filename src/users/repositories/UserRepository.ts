import User from "../models/User";
import UserSchema from "../schemas/UserSchema";
import IRepository from "../../shared/repository/IRepository";
import Repository from "../../shared/repository/Repository";

export interface IUserRepository extends IRepository<User> {
    getByEmail(email: string): Promise<User>;
}

export default class UserRepository extends Repository<User> implements IUserRepository {
    constructor() {
        super(UserSchema);
    }

    public async getByEmail(email: string): Promise<User> {
        const user = await UserSchema.findOne({ email });

        return user ? user.toObject() : null;
    }
}