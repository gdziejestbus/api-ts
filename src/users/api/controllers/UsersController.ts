import { Response, Request } from "express";
import UserService, { IUserService } from "../../services/UserService";
import { toUserDTO } from "../../converters/toUserDTO";
import { errorHandler } from "../../../shared/errors/errorHandler";
import { GetUserByIdRequest, CreateUserRequest, UpdateUserRequest, UpdatePasswordRequest, UpdateAmbassadorRequest } from "../../DTO/UserRequestsDTO";
import { GetUserByIdResponse, CreateUserResponse } from "../../DTO/UserResponsesDTO";
import User from "../../models/User";
import UpdateAmbassador from "../../models/UpdateAmbassador";

export interface IUserController {
    createUser(req: Request, res: Response): Promise<Response>;
    getById(req: Request, res: Response): Promise<Response>;
    deleteById(req: Request, res: Response): Promise<Response>;
    updateUser(req: Request, res: Response): Promise<Response>;
    updatePassword(req: Request, res: Response): Promise<Response>;
    updateAmbassador(req: Request, res: Response): Promise<Response>;
}

export default class UserController implements IUserController {
    private userService: IUserService;

    constructor() {
        this.userService = new UserService();
    }

    public getById = async (req: Request, res: Response): Promise<Response> => {
        const request: GetUserByIdRequest = { id: req.params.userId };

        try {
            const user = await this.userService.getById(request.id);

            const userDTO = toUserDTO(user);

            const response: GetUserByIdResponse = { user: userDTO };

            return res.status(200).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public createUser = async (req: Request, res: Response): Promise<Response> => {
        const request: CreateUserRequest = { ...req.body };

        const user: User = { ...request };

        try {
            const userId = await this.userService.createUser(user);

            const response: CreateUserResponse = { userId };

            return res.status(201).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public deleteById = async (req: Request, res: Response): Promise<Response> => {
        const request: GetUserByIdRequest = { id: req.params.userId };

        try {
            await this.userService.deleteById(request.id);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public updateUser = async (req: Request, res: Response): Promise<Response> => {
        const request: UpdateUserRequest = { ...req.body, id: req.user.id };

        const user: User = { ...request };

        try {
            await this.userService.updateUser(user);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public updatePassword = async (req: Request, res: Response): Promise<Response> => {
        const request: UpdatePasswordRequest = {
            id: req.user.id,
            oldPassword: req.body.oldPassword,
            newPassword: req.body.newPassword
        }

        try {
            await this.userService.updatePassword(request.id, request.oldPassword, request.newPassword);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public updateAmbassador = async (req: Request, res: Response): Promise<Response> => {
        const request: UpdateAmbassadorRequest = {
            ...req.body,
            id: req.user.id
        }

        const ambassador: UpdateAmbassador = { ...request };

        try {
            await this.userService.updateAmbassador(ambassador);

            return res.sendStatus(204);
        } catch (err) {
            return errorHandler(res, err);
        }
    }
}