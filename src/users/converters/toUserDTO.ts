import UserDTO from "../DTO/UserDTO";

export function toUserDTO(user: any): UserDTO {
    const userDTO: UserDTO = {
        id: user.id,
        login: user.login,
        email: user.email,
        ...(user.city && { city: user.city }),
        ...(user.birthDate && { birthDate: user.birthDate }),
        ...(user.companies && { companies: user.companies }),
        ...(user.firstName && { firstName: user.firstName }),
        ...(user.surname && { surname: user.surname }),
        ...(user.status && { status: user.status }),
        ...(user.favourites && { favourites: user.favourites }),
        ...(user.zipCode && { zipCode: user.zipCode }),
        ...(user.street && { street: user.street }),
        ...(user.promoCode && { promoCode: user.promoCode }),
        ...(user.stats && { stats: user.stats })
    }

    return userDTO;
}