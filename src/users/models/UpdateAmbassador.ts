export default interface UpdateAmbassador {
    readonly id: string;
    readonly city: string;
    readonly zipCode: string;
    readonly street: string;
    readonly firstName: string;
    readonly surname: string;
}