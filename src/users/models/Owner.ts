import User from "./User";

export default interface Owner extends User {
    readonly companies: string[];
    readonly firstName?: string;
    readonly surname?: string;
}