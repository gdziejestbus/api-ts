export default interface User {
    readonly id?: string;
    readonly login: string;
    readonly password?: string;
    readonly email: string;
    readonly role?: string;
    readonly city?: string;
    readonly birthDate?: Date;
}