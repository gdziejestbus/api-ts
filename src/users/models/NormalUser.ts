import User from "./User";

export default interface NormalUser extends User {
    readonly status?: string;
    readonly favourites?: {
        readonly routes?: string[];
        readonly stops?: string[];
        readonly companies?: string[];
    };
}