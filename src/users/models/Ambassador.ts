import User from "./User";

export default interface Ambassador extends User {
    readonly city: string;
    readonly zipCode: string;
    readonly street: string;
    readonly firstName: string;
    readonly surname: string;
    readonly promoCode?: string;
    readonly stats?: {
        readonly promoCodeUses: number;
        readonly addedRoutes: number;
        readonly confirmedRoutes: number;
    };
}