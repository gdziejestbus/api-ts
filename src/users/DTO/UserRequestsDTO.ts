export interface GetUserByIdRequest {
    readonly id: string;
}

export interface CreateUserRequest {
    readonly login: string;
    readonly email: string;
    readonly password: string;
}

export interface UpdateUserRequest {
    readonly id: string;
    readonly login: string;
    readonly email: string;
    readonly city?: string;
    readonly birthDate?: Date;
}

export interface UpdatePasswordRequest {
    readonly id: string;
    readonly oldPassword: string;
    readonly newPassword: string;
}

export interface UpdateAmbassadorRequest {
    readonly id: string;
    readonly city: string;
    readonly zipCode: string;
    readonly street: string;
    readonly firstName: string;
    readonly surname: string;
}