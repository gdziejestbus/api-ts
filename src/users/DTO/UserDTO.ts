export default interface UserDTO {
    readonly id: string;
    readonly login: string;
    readonly email: string;
    readonly city?: string;
    readonly birthDate?: Date;
    readonly companies?: string[];
    readonly firstName?: string;
    readonly surname?: string;
    readonly status?: string;
    readonly favourites?: {
        readonly routes?: string[];
        readonly stops?: string[];
        readonly companies?: string[];
    };
    readonly zipCode?: string;
    readonly street?: string;
    readonly promoCode?: string;
    readonly stats?: {
        readonly promoCodeUses?: number;
        readonly addedRoutes?: number;
        readonly confirmedRoutes?: number;
    };
}