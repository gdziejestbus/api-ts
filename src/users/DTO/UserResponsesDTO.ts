import UserDTO from "./UserDTO";

export interface GetUserByIdResponse {
    readonly user: UserDTO
}

export interface CreateUserResponse {
    readonly userId: string;
}