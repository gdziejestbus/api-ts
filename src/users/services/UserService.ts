import NormalUser from "../models/NormalUser";
import Owner from "../models/Owner";
import Ambassador from "../models/Ambassador";
import UserRepository, { IUserRepository } from "../repositories/UserRepository";
import NormalUserRepository, { INormalUserRepository } from "../repositories/NormalUserRepository";
import OwnerRepository, { IOwnerRepository } from "../repositories/OwnerRepository";
import AmbassadorRepository, { IAmbassadorRepository } from "../repositories/AmbassadorRepository";
import User from "../models/User";
import bcrypt from 'bcrypt';
import randomstring from 'randomstring';
import UpdateAmbassador from "../models/UpdateAmbassador";

export interface IUserService {
    createUser(user: User): Promise<string>;
    getById(id: string): Promise<NormalUser | Owner | Ambassador | User>;
    getByEmail(email: string): Promise<User>;
    deleteById(id: string): Promise<void>;
    updateUser(user: User): Promise<void>;
    updatePassword(id: string, oldPassword: string, newPassword: string): Promise<void>;
    updateAmbassador(ambassador: UpdateAmbassador): Promise<void>;
}

export default class UserService implements IUserService {
    private userRepository: IUserRepository;
    private normalUserRepository: INormalUserRepository;
    private ownerRepository: IOwnerRepository;
    private ambassadorRepository: IAmbassadorRepository;

    private SALT_ROUNDS = 10;

    constructor() {
        this.userRepository = new UserRepository();
        this.normalUserRepository = new NormalUserRepository();
        this.ownerRepository = new OwnerRepository();
        this.ambassadorRepository = new AmbassadorRepository();
    }

    public async getById(id: string): Promise<NormalUser | Owner | Ambassador | User> {
        const user = await this.userRepository.getById(id);

        if (!user) throw new Error("NotFound");

        if (user.role === "normal") {
            const normalUser = await this.normalUserRepository.getById(id);

            const userToReturn: NormalUser = { ...user, ...normalUser };

            return userToReturn;
        } else if (user.role === "owner") {
            const owner = await this.ownerRepository.getById(id);

            const userToReturn: Owner = { ...user, ...owner };

            return userToReturn;
        } else if (user.role === "ambassador") {
            const ambassador = await this.ambassadorRepository.getById(id);

            const userToReturn: Ambassador = { ...user, ...ambassador };

            return userToReturn;
        } else {
            return user;
        }
    }

    public async getByEmail(email: string): Promise<User> {
        return await this.userRepository.getByEmail(email);
    }

    public async createUser(user: User): Promise<string> {
        const existingUser = await this.userRepository.getByEmail(user.email);

        if (existingUser) throw new Error("Exist");

        const hash = await bcrypt.hash(user.password, this.SALT_ROUNDS);

        const userWithHash: User = {
            ...user,
            password: hash
        }

        const createdUser = await this.userRepository.create(userWithHash);

        return createdUser.id;
    }

    public async deleteById(id: string): Promise<void> {
        const user = await this.userRepository.getById(id);

        if (!user) return;

        await this.userRepository.deleteById(id);

        if (user.role === "normal") {
            await this.normalUserRepository.deleteById(id);
        } else if (user.role === "owner") {
            await this.ownerRepository.deleteById(id);
        } else if (user.role === "ambassador") {
            await this.ambassadorRepository.deleteById(id);
        }
    }

    public async updateUser(user: User): Promise<void> {
        await this.userRepository.update(user);
    }

    public async updateAmbassador(ambassador: UpdateAmbassador): Promise<void> {
        const user = await this.userRepository.getById(ambassador.id);
        const existingAmbassador = await this.ambassadorRepository.getById(ambassador.id);

        if (existingAmbassador) {
            const updatedAmbassador: Ambassador = {
                ...user,
                ...ambassador
            }

            await this.ambassadorRepository.update(updatedAmbassador);
        } else {
            const newAmbassador: Ambassador = {
                ...user,
                ...ambassador,
                promoCode: randomstring.generate(7)
            }

            await this.ambassadorRepository.create(newAmbassador);
        }
    }

    public async updatePassword(id: string, oldPassword: string, newPassword: string): Promise<void> {
        const user = await this.userRepository.getById(id);

        if (!user) throw new Error("NotFound");

        const checkPassword = await bcrypt.compare(oldPassword, user.password);

        if (checkPassword) {
            const hash = await bcrypt.hash(newPassword, this.SALT_ROUNDS);

            const updatedUser: User = {
                ...user,
                password: hash
            }

            await this.userRepository.update(updatedUser);
        } else {
            throw new Error("WrongCredentials");
        }
    }
}