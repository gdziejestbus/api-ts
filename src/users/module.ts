import UserController from "./api/controllers/UsersController";
import { Router } from "express";

import { createUserValidation, updateUserValidation, updatePasswordValidation, updateAmbassadorValidation } from "./validation/UsersRequestValidation";
import validate from 'express-validation';
import { authMiddleware } from "../shared/middlewares/AuthMiddleware";

const userController = new UserController();

const routes: Router = Router();

routes.get('/:userId', userController.getById);
routes.post('/create', validate(createUserValidation), userController.createUser);
routes.post('/update/user', authMiddleware(["normal", "owner", "ambassador"]), validate(updateUserValidation), userController.updateUser);
routes.post('/update/password', authMiddleware(["normal", "owner", "ambassador"]), validate(updatePasswordValidation), userController.updatePassword);
routes.post('/update/ambassador', authMiddleware(["ambassador"]), validate(updateAmbassadorValidation), userController.updateAmbassador);
routes.delete('/:userId', userController.deleteById);

export { routes };