export interface LoginUserResponse {
    readonly token: string;
    readonly role: string;
}