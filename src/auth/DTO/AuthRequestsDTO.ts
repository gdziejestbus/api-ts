export interface LoginUserRequest {
    readonly email: string;
    readonly password: string;
}

export interface AuthorizationRequest {
    readonly role: string;
    readonly id: string;
}