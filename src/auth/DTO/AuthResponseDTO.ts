export default interface AuthResponseDTO {
    readonly token: string;
    readonly role: string;
}