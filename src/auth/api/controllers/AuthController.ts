import { Response, Request } from "express";
import AuthService, { IAuthService } from "../../services/AuthService";
import { LoginUserRequest, AuthorizationRequest } from "../../DTO/AuthRequestsDTO";
import { toAuthResponseDTO } from "../../converters/toAuthResponseDTO";
import { LoginUserResponse } from "../../DTO/AuthResponsesDTO";
import { errorHandler } from "../../../shared/errors/errorHandler";
import AuthResponse from "../../models/AuthResponse";

export interface IAuthController {
    login(req: Request, res: Response): Promise<Response>;
    auth(req: Request, res: Response): Promise<Response>;
}

export default class AuthController implements IAuthController {
    private authService: IAuthService;

    constructor() {
        this.authService = new AuthService();
    }

    public login = async (req: Request, res: Response): Promise<Response> => {
        const request: LoginUserRequest = {
            email: req.body.email,
            password: req.body.password
        };

        try {
            const authResponse = await this.authService.login(request.email, request.password);

            this.sendAuthResponse(res, authResponse);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public auth = async (req: Request, res: Response): Promise<Response> => {
        const request: AuthorizationRequest = {
            id: req.user.id,
            role: req.user.role
        }

        try {
            const authResponse = await this.authService.auth(request);

            this.sendAuthResponse(res, authResponse);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    private sendAuthResponse = async (res: Response, authResponse: AuthResponse): Promise<Response> => {
        const authResponseDTO = toAuthResponseDTO(authResponse);

        const response: LoginUserResponse = {
            token: authResponseDTO.token,
            role: authResponseDTO.role
        }

        // temp disabled secure cookies
        // res.cookie("token", response.token, { httpOnly: true, expires: config.JWT_EXPIRE_TIME, secure: true });
        res.cookie("token", response.token);
        return res.status(200).json(response);
    }
}