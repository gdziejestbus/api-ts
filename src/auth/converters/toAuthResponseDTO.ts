import AuthResponse from "../models/AuthResponse";
import AuthResponseDTO from "../DTO/AuthResponseDTO";

export const toAuthResponseDTO = (authResponse: AuthResponse): AuthResponseDTO => {
    const authResponseDTO: AuthResponseDTO = {
        token: authResponse.token,
        role: authResponse.role
    }

    return authResponseDTO;
}