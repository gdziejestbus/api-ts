import AuthController from "./api/controllers/AuthController";
import { Router } from "express";

import { loginUserValidation } from "./validation/AuthRequestsValidation";
import validate from 'express-validation';
import { authMiddleware } from "../shared/middlewares/AuthMiddleware";

const authController = new AuthController();

const routes: Router = Router();

routes.get('/', authMiddleware([]), authController.auth);
routes.post('/login', validate(loginUserValidation), authController.login);

export { routes };