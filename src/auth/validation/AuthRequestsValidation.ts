const Joi = require("joi");
import config from '../../shared/config';

export const loginUserValidation = {
    body: {
        password: Joi.string().regex(config.PASSWORD_REGEX).required(),
        email: Joi.string().regex(config.EMAIL_REGEX).required()
    }
}