import AuthResponse from "../models/AuthResponse";
import UserService, { IUserService } from "../../users/services/UserService";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import config from '../../shared/config';
import AuthRequest from "../models/AuthRequest";
import User from "../../users/models/User";

export interface IAuthService {
    login(email: string, password: string): Promise<AuthResponse>;
    auth(decoded: AuthRequest): AuthResponse;
}

export default class AuthService implements IAuthService {
    private userService: IUserService;

    constructor() {
        this.userService = new UserService();
    }

    public async login(email: string, password: string): Promise<AuthResponse> {
        const user = await this.userService.getByEmail(email);

        if (!user) throw new Error("WrongCredentials");

        const checkPassword = await bcrypt.compare(password, user.password);

        if (!checkPassword) {
            throw new Error("WrongCredentials");
        }

        return this.generateToken(user);
    }

    public auth(decoded: AuthRequest): AuthResponse {
        return this.generateToken(decoded);
    }

    private generateToken(authRequest: AuthRequest | User): AuthResponse {
        const token = jwt.sign({
            id: authRequest.id,
            role: authRequest.role
        }, config.JWT_SECRET_KEY, { expiresIn: '1h' });

        const authResponse: AuthResponse = {
            token,
            role: authRequest.role
        }

        return authResponse;
    }
}