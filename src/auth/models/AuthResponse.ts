export default interface AuthResponse {
    readonly token: string;
    readonly role: string;
}