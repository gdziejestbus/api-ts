export default interface AuthRequest {
    readonly role: string;
    readonly id: string;
}