import Statistics from "../models/Statistics";
import StatisticsRepository, { IStatisticsRepository } from "../repositories/StatisticsRepository";

export interface IStatisticsService {
	addStatistics(device: string, browser: string, action: string, details: Object): Promise<void>;
}

export default class StatisticsService implements IStatisticsService {
	private statisticsRepository: IStatisticsRepository;

	constructor() {
		this.statisticsRepository = new StatisticsRepository();
	}

	public async addStatistics(device: string, browser: string, action: string, details: Object): Promise<void> {
		const statistics: Statistics = {
			device,
			action,
			browser,
			details,
			date: new Date()
		}

		await this.statisticsRepository.create(statistics);
	}
}