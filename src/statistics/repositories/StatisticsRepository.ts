import Statistics from "../models/Statistics";
import StatisticsSchema from "../schemas/StatisticsSchema";

export interface IStatisticsRepository {
	create(statistics: Statistics): Promise<void>;
}

export default class StatisticsRepository implements IStatisticsRepository {
	public async create(statistics: Statistics): Promise<void> {
		await StatisticsSchema.create(statistics);
	}
}