export default interface Statistics {
	readonly id?: string;
	readonly device: string;
	readonly action: string;
	readonly browser: string;
	readonly date: Date;
	readonly details: Object;
}