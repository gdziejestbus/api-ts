import { Document, model, Schema } from 'mongoose';

export interface IStatisticsSchema extends Document {
	readonly device: string;
	readonly action: string;
	readonly browser: string;
	readonly date: Date;
	readonly details: Object;
}

const StatisticsSchema: Schema = new Schema({
	device: String,
	action: String,
	browser: String,
	date: Date,
	details: Object
});

StatisticsSchema.virtual('id').get(function () {
	return this._id.toHexString();
});

StatisticsSchema.set('toObject', {
	virtuals: true
});

export default model<IStatisticsSchema>("Statistics", StatisticsSchema);