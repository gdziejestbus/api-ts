import { Router } from "express";
import ImageController from "./api/controller/ImageController";
import multer from 'multer';

const inMemoryStorage = multer.memoryStorage();
const uploadStrategy = multer({ storage: inMemoryStorage }).single('image');

const imageController = new ImageController();

const routes: Router = Router();

routes.post('/upload', uploadStrategy, imageController.uploadImage);
routes.get('/:imageName', uploadStrategy, imageController.getImage);

export { routes };