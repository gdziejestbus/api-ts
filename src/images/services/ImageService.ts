import { Aborter, BlobURL, BlockBlobURL, ContainerURL, ServiceURL, StorageURL, SharedKeyCredential, uploadStreamToBlockBlob, downloadBlobToBuffer } from '@azure/storage-blob';
import randomstring from 'randomstring';
import CONFIG from '../../shared/config';
import getStream from 'into-stream';
import sharp from 'sharp';

export interface IImageService {
    uploadImage(file: any): Promise<string>;
    downloadImage(imageName: string, size: string): Promise<Buffer>;
}

export default class ImageService implements IImageService {
    private ONE_MEGABYTE = 1024 * 1024;
    private uploadOptions = { bufferSize: 4 * this.ONE_MEGABYTE, maxBuffers: 20 };
    private ONE_MINUTE = 60 * 1000;

    private sharedKeyCredential = new SharedKeyCredential(CONFIG.AZURE_STORAGE_ACCOUNT_NAME, CONFIG.AZURE_STORAGE_ACCOUNT_ACCESS_KEY);
    private pipeline = StorageURL.newPipeline(this.sharedKeyCredential);
    private serviceURL = new ServiceURL(CONFIG.AZURE_CONNECTION_STRING, this.pipeline);
    private aborter;

    private SIZES = [64, 128, 256, 512];

    public uploadImage = async (file: Express.Multer.File): Promise<string> => {
        const fileExtension = file.originalname.split(".").pop();
        const blobName = this.getBlobName(fileExtension);

        for (const size of this.SIZES) {
            const image = await this.resizeImage(file, size);

            await this.uploadImageToAzure(image, size, blobName);
            if (this.SIZES.indexOf(size) === this.SIZES.length - 1) return blobName;
        }
    }

    public downloadImage = async (imageName: string, size: string): Promise<Buffer> => {
        let buffer: Buffer = Buffer.alloc(this.calculateBufforSize(size));
        this.aborter = Aborter.timeout(30 * this.ONE_MINUTE);

        let containerURL;
        if (this.SIZES.includes(parseInt(size))) {
            containerURL = ContainerURL.fromServiceURL(this.serviceURL, `${size}x${size}`);
        } else {
            containerURL = ContainerURL.fromServiceURL(this.serviceURL, `512x512`);
        }

        const blobURL = BlobURL.fromContainerURL(containerURL, imageName);

        try {
            await downloadBlobToBuffer(this.aborter, buffer, blobURL, 0);

            return buffer;
        } catch (err) {
            if (err.statusCode === 404) throw new Error("NotFound");

            console.log(err);
            throw new Error("Downloading image failed");
        }
    }

    private calculateBufforSize = (size): number => {
        return this.SIZES.includes(size) ? Math.pow(parseInt(size), 2) * 8 : Math.pow(512, 2) * 8;
    }

    private getBlobName = (extenstion): string => {
        return `${randomstring.generate()}.${extenstion}`;
    };

    private async resizeImage(image, size): Promise<Buffer> {
        return await sharp(image.buffer).resize(size).toBuffer();
    }

    private async uploadImageToAzure(image, size, name): Promise<void> {
        const stream = getStream(image);
        const containerURL = ContainerURL.fromServiceURL(this.serviceURL, `${size}x${size}`);
        const blobURL = BlobURL.fromContainerURL(containerURL, name);
        const blockBlobURL = BlockBlobURL.fromBlobURL(blobURL);

        try {
            await uploadStreamToBlockBlob(
                this.aborter,
                stream,
                blockBlobURL,
                this.uploadOptions.bufferSize,
                this.uploadOptions.maxBuffers
            );
        } catch (err) {
            console.log(err);
            throw new Error("Uploading image failed");
        }
    }
}