import { Response, Request } from "express";
import ImageService, { IImageService } from "../../services/ImageService";
import { errorHandler } from "../../../shared/errors/errorHandler";
import { UploadImageResponse } from "../../DTO/ImageResponseDTO";
import { DownloadImageRequest } from "../../DTO/ImageRequestDTO";

export interface IImageController {
    uploadImage(req: Request, res: Response): Promise<Response>;
}

export default class ImageController implements IImageController {
    private imageService: IImageService;

    constructor() {
        this.imageService = new ImageService();
    }

    public uploadImage = async (req: Request, res: Response): Promise<Response> => {
        try {
            const imageName = await this.imageService.uploadImage(req.file);

            const response: UploadImageResponse = {
                imageName
            }

            res.status(200).json(response);
        } catch (err) {
            return errorHandler(res, err);
        }
    }

    public getImage = async (req: Request, res: Response): Promise<Response> => {
        const request: DownloadImageRequest = {
            size: req.query.size,
            imageName: req.params.imageName
        }

        try {
            const image = await this.imageService.downloadImage(request.imageName, request.size);

            res.status(200).type("png").send(image);
        } catch (err) {
            return errorHandler(res, err);
        }
    }
}