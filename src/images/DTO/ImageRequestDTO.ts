export interface DownloadImageRequest {
    size: string;
    imageName: string;
}